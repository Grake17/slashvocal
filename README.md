# 🤖 | SOT Italia Discord Community
This app add:<br>
-- XP/Ranking system to discord server.<br>
-- XP/Talleri rewards for game progress (Achivement/Stats) 
## 📂 | Installation
This app use [NPM](https://www.npmjs.com/) as **pm**, so for install tipe the following command.
```
git clone https://gitlab.com/Grake17/slashvocal.git
npm i
```
## 📜 | Requirements
Postgres DB is require.
## 🔨 | Build
This app use [Typescript](https://www.typescriptlang.org/) and [fp-ts](https://github.com/gcanti) so for build run the command below.
```
tsc
```
## ⚙️ | Run
```
nodemon dist/main.js
```
## 👥 | Collaborators
@Daniele_s, @totto96, @Grake17
