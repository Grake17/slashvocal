module.exports = {
  apps: [
    {
      name: "Cotton_XP_BOT",
      script: "dist/app.js",
      instances: 1,
      autorestart: true,
      watch: true,
      ignore_watch: ["node_modules", ".config.json", ".db.json", "DBBackups"],
    },
  ],
};
