// Import -----------------------------

import { Context } from "../Config/Contex";
import { _remVoice } from "../Utils/Utils-LevelXP/setVoice";
import { _setVoice } from "../Utils/Utils-LevelXP/setVoice";
import { VoiceState } from "discord.js";

// Export ---------------------------

export async function _voiceHandler(
  OldMember: VoiceState,
  NewMember: VoiceState,
  contex: Context
) {
  // Check bot
  if (NewMember.member?.user.bot == true) return;
  if (OldMember.member?.user.bot == true) return;
  // Set Last
  if (NewMember.channel) return _setVoice(NewMember.id)(contex)();
  // Rem Last
  if (OldMember.channel && !NewMember.channelID)
    return _remVoice(OldMember.id)(contex)();
}
