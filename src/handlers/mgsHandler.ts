// import -----------------------------

import { Context } from "../Config/Contex";
import { cmds } from "../command/cmdList";
import { _err } from "../Utils/Utils-Err&Log/ErrorCMD";
import { _embedLog, _sendLog } from "../Utils/Utils-Err&Log/Log";
import { Message, TextChannel } from "discord.js";
import { constVoid, pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _setMgs } from "../Utils/Utils-LevelXP/setMgs";
import * as config from "../config.json";
import { unknown } from "io-ts";

// function ---------------------------

const run_cmds = (msg: Message, context: Context) =>
  pipe(
    RTE.of(cmds),
    RTE.chain(
      RTE.traverseArray((cmd) =>
        pipe(
          RTE.of(msg.content.split(" ")[0].substring(1).toLowerCase()),
          RTE.chain((cmd_name: string) =>
            cmd.name !== cmd_name
              ? RTE.of(constVoid)
              : pipe(
                  cmd.execute(msg, context),
                  RTE.map(() => constVoid)
                )
          )
        )
      )
    )
  );

// Export -----------------------------

export const _msgHandler = (msg: Message, context: Context) =>
  pipe(
    RTE.of(msg),
    RTE.chain((msg: Message) =>
      msg.author.bot === true ||
      !msg.content.startsWith(context.prefix) ||
      msg.guild === null ||
      !config.BOT_Data.CMD_Channel.CMD_ID.includes(msg.channel.id)
        ? RTE.of(constVoid)
        : pipe(
            run_cmds(msg, context),
            RTE.map(
              (): TextChannel =>
                msg.guild!.channels.cache.get(
                  config.BOT_Data.LOG_Channel.LOG_ID
                ) as TextChannel
            ),
            RTE.chainTaskK((logChannel: TextChannel) =>
              _embedLog(logChannel, msg)
            ),
            RTE.map(() => constVoid)
          )
    ),
    RTE.mapLeft((err) => _err(err, msg)(unknown)())
  );
