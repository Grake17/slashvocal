// import -------------------------

import { Guild, GuildChannel, TextChannel, VoiceChannel } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../../Interface/Error";
import { _APIDiscordErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// function -----------------------

const _getDiscordChannel = (
  guild: Guild,
  id: string
): TE.TaskEither<errorBOT, GuildChannel> =>
  pipe(
    TE.of(guild.channels.cache.get(id)),
    TE.chain(
      TE.fromPredicate(
        (channel): channel is GuildChannel => typeof channel !== "undefined",
        (channel): errorBOT => _APIDiscordErrorBOT(`canale`, channel)
      )
    )
  );

// export -------------------------

export const _getDiscordTextChannel = (
  guild: Guild,
  id: string
): TE.TaskEither<errorBOT, TextChannel> =>
  pipe(
    _getDiscordChannel(guild, id),
    TE.map((channel: GuildChannel) => channel as TextChannel)
  );

export const _getDiscordVoiceChannel = (
  guild: Guild,
  id: string
): TE.TaskEither<errorBOT, VoiceChannel> =>
  pipe(
    _getDiscordChannel(guild, id),
    TE.map((channel: GuildChannel) => channel as VoiceChannel)
  );
