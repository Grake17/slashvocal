// import --------------------------------

import { Guild, GuildMember } from "discord.js";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../../Interface/Error";
import { _APIDiscordErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export --------------------------------

export const _getDiscordUser = (
  guild: Guild,
  id: string
): TE.TaskEither<errorBOT, GuildMember> =>
  TE.tryCatch(
    async () => guild.members.fetch(id),
    (err): errorBOT => _APIDiscordErrorBOT(`utente`, err)
  );
