// import ----------------------------

import { Message, MessageAttachment, MessageEmbed } from "discord.js";
import * as T from "fp-ts/lib/Task";

// export ----------------------------

export const _sendEmbed =
  (msg: Message, embed: MessageEmbed | MessageAttachment): T.Task<Message> =>
  () =>
    msg.channel.send(embed);

export const _sendAttachment =
  (msg: Message, attachment: MessageAttachment): T.Task<Message> =>
  () =>
    msg.channel.send(attachment);
