// import --------------------------------

import { Guild, GuildMember, Role } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../../Interface/Error";
import { _APIDiscordErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export --------------------------------

// Add specified Role to the pass GuildMember
export const _addDiscordRole = (user: GuildMember, role: Role) =>
  TE.tryCatch(
    async () => user.roles.add(role),
    (err): errorBOT => _APIDiscordErrorBOT(`utente`, err)
  );

// Remove specified Role to the pass GuildMember
export const _remDiscordRole = (user: GuildMember, role: Role) =>
  TE.tryCatch(
    async () => user.roles.remove(role),
    (err): errorBOT => _APIDiscordErrorBOT(`utente`, err)
  );

// Check Has Specified Role to the pass User
export const _hasDiscordRole = (user: GuildMember, id: string): boolean =>
  user.roles.cache.has(id);

// Fetch Discord Role on the pass Guild
export const _getDiscordRole = (
  guild: Guild,
  id: string
): TE.TaskEither<errorBOT, Role> =>
  pipe(
    TE.tryCatch(
      async () => guild.roles.fetch(id),
      (err): errorBOT => {
        return {
          name: "Discord API Error",
          description: `Can't fetch role <@!${id}>`,
          rank: 1,
          errCode: err,
        };
      }
    ),
    TE.chain<errorBOT, Role | null, Role>(
      TE.fromPredicate(
        (role): role is Role => typeof role !== null,
        (err): errorBOT => _APIDiscordErrorBOT(`ruolo`, err)
      )
    )
  );
