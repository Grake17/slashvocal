// import --------------------------------

import { Guild } from "discord.js";
import * as TE from "fp-ts/lib/TaskEither";
import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import { _APIDiscordErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export --------------------------------

export const _getDiscordGuild = (
  context: Context,
  id: string
): TE.TaskEither<errorBOT, Guild> =>
  TE.tryCatch(
    async () => context.client.guilds.fetch(id),
    (err): errorBOT => _APIDiscordErrorBOT(`server`, err)
  );
