// import -------------------------------------

import { constVoid, pipe } from "fp-ts/lib/function";
import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import * as TE from "fp-ts/lib/TaskEither";
import * as T from "fp-ts/lib/Task";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Guild, Message, MessageEmbed, TextChannel } from "discord.js";
import * as config from "../../config.json";
import { _getDiscordGuild } from "../Utils-DiscordAPI/guildDiscordAPI";
import { _getDiscordTextChannel } from "../Utils-DiscordAPI/channelDiscordAPI";
import { _sendEmbed } from "../Utils-DiscordAPI/embedDiscordAPI";
import { _sendLog } from "./Log";

// function -----------------------------------

const _sendEmbedJob = (channel: TextChannel, err: errorBOT): T.Task<Message> =>
  pipe(
    T.of(constVoid),
    T.map(() => {
      const embed = new MessageEmbed()
        .setAuthor(config.BOT_Data.author)
        .setColor(config.BOT_Data.LOG_Channel.LOG_Colors[err.rank])
        .setTitle(`JOB Error`)
        .setDescription(`${err.description}\n${String(err.errCode)}`);
      return embed;
    }),
    T.chain((embed: MessageEmbed) => () => channel.send(embed))
  );

// export -------------------------------------

export const _errorJob = (err: errorBOT) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      pipe(
        _getDiscordGuild(context, context.env.GUILD_ID),
        TE.chain((guild: Guild) =>
          _getDiscordTextChannel(guild, config.BOT_Data.LOG_Channel.LOG_ID)
        )
      )
    ),
    RTE.chainTaskK((channel: TextChannel) => _sendEmbedJob(channel, err))
  );
