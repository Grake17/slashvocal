// import ----------------------------

import { Guild, Message, MessageEmbed, TextChannel } from "discord.js";
import { constVoid, pipe } from "fp-ts/lib/function";
import * as TE from "fp-ts/lib/TaskEither";
import * as IO from "fp-ts/lib/IO";
import * as T from "fp-ts/lib/Task";
import * as config from "../../config.json";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _getDiscordTextChannel } from "../Utils-DiscordAPI/channelDiscordAPI";

// function ----------------------------

export const _embedLog = (
  channel: TextChannel,
  msg: Message
): T.Task<Message> => {
  const text = [
    `👥 Log of user: <@!${msg.author.id}>`,
    `💌 Message content: ${msg.content}`,
    `📩 Message Link: ${msg.url}`,
  ].join("\n");
  const embed = new MessageEmbed()
    .setAuthor(config.BOT_Data.author)
    .setColor(config.BOT_Data.LOG_Channel.LOG_Colors[0])
    .setDescription(text);
  return () => channel.send(embed);
};

// export -------------------------------

export const _sendLog = (msg: Message) =>
  pipe(
    RTE.of(msg.guild),
    RTE.chain((guild: Guild | null) =>
      guild === null
        ? () => TE.of(constVoid)
        : () =>
            pipe(
              _getDiscordTextChannel(guild, config.BOT_Data.LOG_Channel.LOG_ID),
              TE.chainTaskK((channel: TextChannel) => _embedLog(channel, msg)),
              TE.mapLeft(
                (err): IO.IO<void> =>
                  () =>
                    console.log(err)
              ),
              TE.map(() => constVoid)
            )
    ),
    RTE.map(() => msg)
  );
