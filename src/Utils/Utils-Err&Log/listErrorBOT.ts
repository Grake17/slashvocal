// import --------------------------

import { User } from "discord.js";
import { levelAttribute } from "../../DB/Models/levelsTable";
import { errorBOT } from "../../Interface/Error";

// export --------------------------

export const _permissionErrorBOT = (user: User): errorBOT => {
  return {
    name: `**ErrorType**: Permission error`,
    description: `${user} non hai i permessi necessari per fare questo comando.`,
    rank: 3,
    errCode: `L'utente: ${user} ha fatto un comando non autorizzato`,
  };
};

export const _apiErrorBOT = (response: unknown): errorBOT => {
  return {
    name: `**ErrorType**: Fetch API Error`,
    description: `Errore nel reperimento delle statistiche`,
    rank: 2,
    errCode: response,
  };
};

export const _getAllDBDataErrorBOT = (job: string, err: unknown): errorBOT => {
  return {
    name: `**ErrorType**: SQL Error`,
    description: `Errore nel reperimento dei dati durante il ${job}`,
    rank: 2,
    errCode: err,
  };
};

export const _upDBDataErrorBOT = (id: string, err: unknown): errorBOT => {
  return {
    name: `**ErrorType**: SQL Error`,
    description: `Errore nell'aggiornamento dei dati`,
    rank: 2,
    errCode: err,
  };
};

export const _getDBDataErrorBOT = (id: string, err: unknown): errorBOT => {
  return {
    name: `**ErrorType**: SQL Error`,
    description: `Errore nel reperimento dei dati dell'utente <@!${id}>`,
    rank: 2,
    errCode: err,
  };
};

export const _APIDiscordErrorBOT = (ctx: string, err: unknown): errorBOT => {
  return {
    name: `**ErrorType**: Discord API Error`,
    description: `Errore nel reperimento: ${ctx}`,
    rank: 2,
    errCode: err,
  };
};

export const _mathErrorBOT = (id: string, err: unknown): errorBOT => {
  return {
    name: `*ErrorType**: Math Error`,
    description: `Errore nel calcolo dell'xp`,
    rank: 2,
    errCode: err,
  };
};

// export const _fsErrorBOT = (err: unknown): errorBOT => {
//   return {
//     name: `*ErrorType**: FS Error`,
//     description: `Errore nella read/write del file di configurazione`,
//     rank: 2,
//     errCode: err,
//   };
// };

export const _canvaErrorBOT = (err: unknown): errorBOT => {
  return {
    name: `**ErrorType**: Canva Error`,
    description: `Errore nel caricamento dell'immagine`,
    rank: 2,
    errCode: err,
  };
};

export const _moneyErrorBot = (id: string): errorBOT => {
  return {
    name: `**ErrorType**: SQL Error`,
    description: `**Ahoy <@!${id}>**, non hai abbastanza talleri!`,
    rank: 1,
    errCode: `Il pirata <@!${id}> è povero`,
  };
};

export const _userRegisterErrotBOT = (user: string, err: unknown): errorBOT => {
  return {
    name: `**ErrorType**: SQL Error`,
    description: `**Ahoy <@!${user}>**, registrati con il comando **$register**`,
    rank: 1,
    errCode: err,
  };
};

export const _syntaxErrorBOT = (text: string): errorBOT => {
  return {
    name: `**ErrorType**: Synstax error`,
    description: `Hai sbagliato il comando!`,
    rank: 1,
    errCode: text,
  };
};

export const _banErrorBOT = (id: string): errorBOT => {
  return {
    name: `**ErrorType**: Ban User`,
    description: `**Ahoy <@!${id}>**, non puoi eseguire comandi in quanto bannato.`,
    rank: 1,
    errCode: `L'utente <@!${id}> fa comandi da **bannato**`,
  };
};

export const _prestigeErrorBOT = (user: User): errorBOT => {
  return {
    name: `**ErrorType**: Prestigio non effettuabile`,
    description: `**Ahoy ${user}**, devi essere al livello **100** per prestigiare!`,
    rank: 1,
    errCode: `L'utente ${user} prova a fare il prestigio senza successo`,
  };
};

export const _syncErrorBOT = (
  id: string,
  regOld: boolean,
  yetSync: boolean
): errorBOT => {
  return {
    name: `**ErrorType**: Sincronizzazioe non effettuabile`,
    description: `**Ahoy <@!${id}>**, sei già aggiornato al nuovo sistema!`,
    rank: 1,
    errCode: `\nUtente sul vecchio sistema: ${regOld}\nUtente già aggiornato: ${yetSync}`,
  };
};

export const _multErrorBOT = (attribute: levelAttribute): errorBOT => {
  return {
    name: `**Numero moltiplicatore non valido**`,
    description: `**Ahoy <@!${attribute.id}>**, hai sbagliato la sintassi!`,
    rank: 1,
    errCode: `\nUtente è uno spendaccione`,
  };
};

export const _multClaimedErrorBot = (attribute: levelAttribute): errorBOT => {
  return {
    name: `**Moltiplicatore già riscattato**`,
    description: `**Ahoy <@!${
      attribute.id
    }>**, hai già comprato troppi il moltiplicatori!`,
    rank: 1,
    errCode: `\nUtente è uno spendaccione`,
  };
};
