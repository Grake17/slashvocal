// import ------------------------------------

import { errorBOT } from "../../Interface/Error";
import * as T from "fp-ts/lib/Task";
import { Guild, Message, MessageEmbed, TextChannel } from "discord.js";
import * as TE from "fp-ts/lib/TaskEither";
import * as config from "../../config.json";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { constVoid, pipe } from "fp-ts/lib/function";
import { _getDiscordTextChannel } from "../Utils-DiscordAPI/channelDiscordAPI";

// function ----------------------------------

const _sendErr = (
  msg: Message,
  channel: TextChannel,
  err: errorBOT
): T.Task<Message> => {
  const text = [
    `👥 Log of user: <@!${msg.author.id}>`,
    `💌 Message content: ${msg.content}`,
    `📩 Message Link: ${msg.url}`,
    `------------------------------------------------------------------------------------`,
    `📜 Error Code: ${err.errCode}`,
  ].join("\n");
  const embed = new MessageEmbed()
    .setTitle(err.name)
    .setAuthor(config.BOT_Data.author)
    .setColor(config.BOT_Data.LOG_Channel.LOG_Colors[err.rank])
    .setDescription(text);
  return () => channel.send(embed);
};

const _sendResponse = (
  channel: TextChannel,
  err: errorBOT
): T.Task<Message> => {
  const embed = new MessageEmbed()
    .setAuthor(config.BOT_Data.author)
    .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
    .setDescription(err.description);
  return () => channel.send(embed);
};

// export ------------------------------------

export const _err = (err: errorBOT, msg: Message) =>
  pipe(
    RTE.of(msg.guild),
    RTE.chain((guild: Guild | null) =>
      guild === null
        ? () => TE.of(constVoid)
        : () =>
            pipe(
              _getDiscordTextChannel(guild, config.BOT_Data.LOG_Channel.LOG_ID),
              TE.chainFirstTaskK((channel: TextChannel) =>
                _sendErr(msg, channel, err)
              ),
              TE.chainFirstTaskK(() =>
                _sendResponse(msg.channel as TextChannel, err)
              ),
              TE.map(() => constVoid)
            )
    )
  );
