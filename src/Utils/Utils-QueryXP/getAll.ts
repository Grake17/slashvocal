// import ----------------------

import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { _getDBDataErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// function --------------------

const query = (id: string) => {
  return pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context) =>
      TE.tryCatch(
        async () => {
          // Create Transaction
          const t = await context.sequelize.transaction();
          // Get ID level data
          const levelResult = await context.tables.level.findOne({
            where: { id: id },
            transaction: t,
          });
          // Get ID userXP data
          const userXPResult = await context.tables.userXP.findOne({
            where: { id: id },
            transaction: t,
          });
          // Commit Query
          await t.commit();
          return { levelResult, userXPResult };
        },
        (err): errorBOT => _getDBDataErrorBOT(id, err)
      )
    ),
    RTE.map((a) => {
      // Check Data Type
      const userXPData = a.userXPResult?.get();
      const levelData = a.levelResult?.get();
      return { userXPData, levelData };
    })
  );
};

// export -----------------------

export const _getAll = (id: string) => {
  return pipe(id, query);
};
