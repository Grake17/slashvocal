// import ----------------------------

import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import { validateObj } from "../../Interface/ObiT";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { _upDBDataErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export -----------------------------

export const _upAll = (attributes: validateObj) => {
  return pipe(
    RTE.Do,
    RTE.bind("context", () => RTE.ask<Context>()),
    RTE.chainTaskEitherK(({ context }) =>
      TE.tryCatch(
        async () => {
          const t = await context.sequelize.transaction();
          await context.tables.userXP.update(
            { ...attributes.userXPData },
            { where: { id: attributes.userXPData.id }, transaction: t }
          );
          await context.tables.level.update(
            { ...attributes.levelData },
            { where: { id: attributes.levelData.id }, transaction: t }
          );
          await t.commit();

          return attributes;
        },
        (err): errorBOT => _upDBDataErrorBOT(attributes.levelData.id, err)
      )
    )
  );
};
