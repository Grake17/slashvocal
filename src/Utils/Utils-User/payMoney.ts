// import ------------------------

import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { userAttribute } from "../../DB/Models/usersTable";
import { _checkMoney } from "../Utils-CheckThings/checkMoney";
import { _getMoney } from "./getMoney";
import { _remMoney } from "./remMoney";

// export ------------------------

export const _payMoney = (id: string, pay: number) =>
  pipe(
    _getMoney(id),
    _checkMoney(pay),
    RTE.chain((attribute: userAttribute) => _remMoney(attribute.userId, pay))
  );
