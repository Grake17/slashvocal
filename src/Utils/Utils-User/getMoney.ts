// import --------------------------

import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../Config/Contex";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../../Interface/Error";
import {
  _getDBDataErrorBOT,
  _userRegisterErrotBOT,
} from "../Utils-Err&Log/listErrorBOT";
import { userAttribute } from "../../DB/Models/usersTable";

// export --------------------------

export const _getMoney = (id: string) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.user.findOne({
              where: { userId: id },
              transaction: t,
            });
            return data;
          });
          return result;
        },
        (err): errorBOT => _getDBDataErrorBOT(id, err)
      )
    ),
    RTE.map((data) => data?.get()),
    RTE.chain<Context, errorBOT, userAttribute | undefined, userAttribute>(
      RTE.fromPredicate(
        (attribute): attribute is userAttribute =>
          typeof attribute !== "undefined",
        (attribute) => _userRegisterErrotBOT(id, attribute)
      )
    )
  );
