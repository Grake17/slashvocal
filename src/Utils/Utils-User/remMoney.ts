import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../Config/Contex";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../../Interface/Error";
import { _upDBDataErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export --------------------------

export const _remMoney = (id: string, n: number) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const user = await context.tables.user.findOne({
              where: { userId: id },
              transaction: t,
            });
            const newSaldo = user!.get().saldo - n;
            const data = await context.tables.user.update(
              { saldo: newSaldo },
              { where: { userId: id }, transaction: t }
            );
            return data;
          });
          return result;
        },
        (err): errorBOT => _upDBDataErrorBOT(id, err)
      )
    )
  );
