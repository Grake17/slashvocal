// import ---------------------------

import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { _userRegisterErrotBOT } from "../Utils-Err&Log/listErrorBOT";

// export ---------------------------

export const _getStats = (id: string) => {
  return pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.userXP.findOne({
              where: { id: id },
              transaction: t,
            });
            return data;
          });
          return result;
        },
        (err): errorBOT => _userRegisterErrotBOT(id, err)
      )
    ),
    RTE.map((data) => data?.get())
  );
};
