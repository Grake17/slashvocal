// import --------------------

import { Context } from "../../Config/Contex";
import { AchivementT, StatsT } from "../../Interface/API";
import { errorBOT } from "../../Interface/Error";
import fetch from "node-fetch";
import * as TE from "fp-ts/lib/TaskEither";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _apiErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export ----------------------

export const _achivement = (xuid: string) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          // Achivement API End Point
          const response = await fetch(
            `https://xbl.io/api/v2/achievements/player/${xuid}/title/${context.env.TITLE_ID}`,
            {
              headers: {
                "X-Authorization": context.env.API_KEY_XBL,
                Accept: "application/json",
              },
            }
          );
          const data: AchivementT = await response.json();
          return {
            achievements: data.achievements,
          };
        },
        (err): errorBOT => _apiErrorBOT(err)
      )
    )
  );

export const _stats = (xuid: string) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          // Stats API End Point
          const result = await fetch(
            `https://xapi.us/v2/${xuid}/game-stats/${context.env.TITLE_ID}`,
            {
              headers: {
                "X-Auth": context.env.API_KEY,
                "Content-Type": "application/json",
              },
            }
          );
          const data: StatsT = await result.json();
          const stats = data.groups[0].statlistscollection[0].stats.map(
            (data) => {
              return { name: data.name, value: data.value };
            }
          );
          return stats;
        },
        (err): errorBOT => _apiErrorBOT(err)
      )
    )
  );
