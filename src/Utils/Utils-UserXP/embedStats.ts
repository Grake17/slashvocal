// import ------------------------

import { MessageEmbed } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as config from "../../config.json";
import { validateObj } from "../../Interface/ObiT";

// export -----------------------

export const _countStats = (
  oldAttributes: validateObj,
  newAttributes: validateObj
): MessageEmbed => {
  const embed = new MessageEmbed()
    .setAuthor(config.BOT_Data.author)
    .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
    .setDescription(`Incassi dell'utente **${newAttributes.userXPData.gt}**`);
  return pipe(
    embed,
    (embed: MessageEmbed) =>
      oldAttributes.userXPData.tresures === newAttributes.userXPData.tresures
        ? embed
        : embed.addField(
            "💰 Tesori incassati",
            `${oldAttributes.userXPData.tresures} -> ${
              newAttributes.userXPData.tresures
            } => XP/Talleri ottenuti: ${
              (newAttributes.userXPData.tresures -
                oldAttributes.userXPData.tresures) *
              config.XP_DATA.XP_RewardGame.XP_Items
            }`
          ),
    (embed: MessageEmbed) =>
      oldAttributes.userXPData.skulls === newAttributes.userXPData.skulls
        ? embed
        : embed.addField(
            "💀 Teschi incassati",
            `${oldAttributes.userXPData.skulls} -> ${
              newAttributes.userXPData.skulls
            } => XP/Talleri ottenuti: ${
              (newAttributes.userXPData.skulls -
                oldAttributes.userXPData.skulls) *
              config.XP_DATA.XP_RewardGame.XP_Items
            }`
          ),
    (embed: MessageEmbed) =>
      oldAttributes.userXPData.loads === newAttributes.userXPData.loads
        ? embed
        : embed.addField(
            "📦 Casse consegnate",
            `${oldAttributes.userXPData.loads} -> ${
              newAttributes.userXPData.loads
            } => XP/Talleri ottenuti: ${
              (newAttributes.userXPData.loads -
                oldAttributes.userXPData.loads) *
              config.XP_DATA.XP_RewardGame.XP_Items
            }`
          ),
    (embed: MessageEmbed) =>
      oldAttributes.userXPData.isles === newAttributes.userXPData.isles
        ? embed
        : embed.addField(
            "🌅 Isole visitate",
            `${oldAttributes.userXPData.isles} -> ${
              newAttributes.userXPData.isles
            } => XP/Talleri ottenuti: ${
              (newAttributes.userXPData.isles -
                oldAttributes.userXPData.isles) *
              config.XP_DATA.XP_RewardGame.XP_Island
            }`
          ),
    (embed: MessageEmbed) =>
      oldAttributes.userXPData.meters === newAttributes.userXPData.meters
        ? embed
        : embed.addField(
            "⛵️ Miglia percorse",
            `${oldAttributes.userXPData.meters} -> ${
              newAttributes.userXPData.meters
            } => XP/Talleri ottenuti: ${Math.round(
              (newAttributes.userXPData.meters -
                oldAttributes.userXPData.meters) *
                config.XP_DATA.XP_RewardGame.XP_Mile
            )}`
          ),
    (embed: MessageEmbed) =>
      oldAttributes.userXPData.voyages === newAttributes.userXPData.voyages
        ? embed
        : embed.addField(
            "🌠 Viaggi completati",
            `${oldAttributes.userXPData.voyages} -> ${
              newAttributes.userXPData.voyages
            } => XP/Talleri ottenuti: ${
              (newAttributes.userXPData.voyages -
                oldAttributes.userXPData.voyages) *
              config.XP_DATA.XP_RewardGame.XP_Vojage
            }`
          ),
    (embed: MessageEmbed) =>
      oldAttributes.userXPData.achievement ===
      newAttributes.userXPData.achievement
        ? embed
        : embed.addField(
            "✅ Obiettivi completati",
            `${oldAttributes.userXPData.achievement} -> ${
              newAttributes.userXPData.achievement
            } => XP/Talleri ottenuti: ${
              (newAttributes.userXPData.achievement -
                oldAttributes.userXPData.achievement) *
              config.XP_DATA.XP_RewardGame.XP_Achivement
            }`
          ),
    (embed: MessageEmbed) =>
      oldAttributes.levelData.xp != newAttributes.levelData.xp
        ? embed
        : embed.setDescription(
            `**${oldAttributes.userXPData.gt}** i mari si *Sea of Thieves* non hanno nulla da dire...\nHai già incassato tutto quanto`
          )
  );
};
