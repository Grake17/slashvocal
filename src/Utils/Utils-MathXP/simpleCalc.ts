// export ----------------------

export const _add = (num1: number, num2: number) => num1 + num2;

export const _rem = (num1: number, num2: number) => num1 - num2;