// import ----------------------------

import * as config from "../../config.json";

// export ----------------------------

export const _calcMile = (num: number) =>
  Math.round(num * config.XP_DATA.XP_RewardGame.XP_Mile);

export const _calcItems = (num: number): number =>
  Math.round(num * config.XP_DATA.XP_RewardGame.XP_Items);

export const _calcVojage = (num: number) =>
  Math.round(num * config.XP_DATA.XP_RewardGame.XP_Vojage);

export const _calcIsland = (num: number) =>
  Math.round(num * config.XP_DATA.XP_RewardGame.XP_Island);
