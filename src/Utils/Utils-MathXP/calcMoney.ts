// import --------------------------

import { pipe } from "fp-ts/lib/function";
import * as config from "../../config.json";
import { levelAttribute } from "../../DB/Models/levelsTable";
import * as A from "fp-ts/lib/Array";

// function ------------------------

const _calcRemMoneyRoles = (arr: string[], newLevel: number): number =>
  pipe(
    config.XP_DATA.XP_Roles,
    A.filter((obj) => arr.includes(obj.ID_Role)),
    A.filter((obj) => newLevel <= obj.LEVEL_Role),
    A.reduceRight(0, (prev, next) => prev.Reward_Role + next)
  );

const _calcAddMoneyRoles = (arr: string[], newLevel: number): number =>
  pipe(
    config.XP_DATA.XP_Roles,
    A.filter((obj) => newLevel >= obj.LEVEL_Role),
    A.filter((obj) => !arr.includes(obj.ID_Role)),
    A.reduceRight(0, (prev, next) => prev.Reward_Role + next)
  );

// export --------------------------

export const _calcAddMoney = (
  oldAttribute: levelAttribute,
  newAttribute: levelAttribute
) =>
  pipe(
    _calcAddMoneyRoles(newAttribute.roles, newAttribute.level),
    // (n) => {
    //   console.log(`XP aggiunta dai ruoli: ${n}`);
    //   return n;
    // },
    (result): number =>
      result +
      (newAttribute.level - oldAttribute.level) *
        (config.Money.Reward_next + newAttribute.rank * 100),
    // (n) => {
    //   console.log(`XP aggiunta dai livelli + ruoli: ${n}`);
    //   console.log(newAttribute.level - oldAttribute.level, oldAttribute, newAttribute)
    //   return n;
    // }
  );

export const _calcRemMoney = (
  oldAttribute: levelAttribute,
  newAttribute: levelAttribute
) =>
  pipe(
    _calcRemMoneyRoles(newAttribute.roles, newAttribute.level),
    (result): number =>
      result +
      (oldAttribute.level - newAttribute.level) *
        (config.Money.Reward_next + newAttribute.rank * 100)
  );
