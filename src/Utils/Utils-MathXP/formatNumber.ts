// export --------------------------------

export const _formatNumber = (n: number): string => new Intl.NumberFormat().format(n)