// import -------------------------

import { _calcLevel } from "./calcLevel";
import * as config from "../../config.json";
import { AchivementT } from "../../Interface/API";
import { validateObj } from "../../Interface/ObiT";
import { _calcNext } from "./calcNext";
import { pipe } from "fp-ts/lib/function";

// function -----------------------

const _xpAddAchivement =
  (xp: number) =>
  (num: number): number =>
    xp + num * config.XP_DATA.XP_RewardGame.XP_Achivement;

const _checkAchivement = (list: AchivementT): number => {
  return list.achievements.filter((a) => a.progressState === "Achieved").length;
};

const _diffAchivement =
  (oldNum: number) =>
  (newNum: number): number =>
    newNum - oldNum;

// export -------------------------

export const _calcAchivement = (
  achivement: AchivementT,
  DataT: validateObj
): validateObj => {
  const newAchivements = _checkAchivement(achivement);
  const newXP = pipe(
    newAchivements,
    _diffAchivement(DataT.userXPData.achievement),
    _xpAddAchivement(DataT.levelData.xp)
  );
  const level = _calcLevel(newXP, DataT.levelData.rank);
  const newNext = _calcNext(level, DataT.levelData.rank);
  return {
    levelData: {
      ...DataT.levelData,
      xp: newXP,
      level: level,
      next: newNext,
    },
    userXPData: {
      ...DataT.userXPData,
      achievement: newAchivements,
    },
  };
};

