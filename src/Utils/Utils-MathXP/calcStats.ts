// import -----------------------

import { StatsObj } from "../../Interface/API";
import { validateObj } from "../../Interface/ObiT";
import { _calcLevel } from "./calcLevel";
import { _calcNext } from "./calcNext";
import { _calcIsland, _calcItems, _calcMile } from "./calcReward";
import { _add } from "./simpleCalc";

// function ---------------------

const _filterStats = (stats: StatsObj, name: string) =>
  stats.filter((obj) => obj.name === name);

const _calcDiff = (stats: StatsObj, num: number, name: string): number => {
  const data = _filterStats(stats, name);
  return data[0].value - num;
};

// export -----------------------

export const _calcStats = (
  stats: StatsObj,
  DataT: validateObj
): validateObj => {
  // Calc Diff Stats Old and New
  const tresure = _calcDiff(stats, DataT.userXPData.tresures, "ChestsCashedIn");
  const skull = _calcDiff(stats, DataT.userXPData.skulls, "BountiesHandedIn");
  const vojage = _calcDiff(stats, DataT.userXPData.voyages, "VoyagesCompleted");
  const cargo = _calcDiff(stats, DataT.userXPData.loads, "CargoDelivered");
  const island = _calcDiff(stats, DataT.userXPData.isles, "IslandsVisited");
  const meter = _calcDiff(stats, DataT.userXPData.meters, "DistanceSailed");
  // Calc XP again
  const newXP =
    _calcItems(cargo + tresure + skull) +
    _calcMile(meter) +
    _calcIsland(island) +
    DataT.levelData.xp;
  const newLevel = _calcLevel(newXP, DataT.levelData.rank);
  return {
    userXPData: {
      ...DataT.userXPData,
      tresures: _add(DataT.userXPData.tresures, tresure),
      skulls: _add(DataT.userXPData.skulls, skull),
      voyages: _add(DataT.userXPData.voyages, vojage),
      isles: _add(DataT.userXPData.isles, island),
      loads: _add(DataT.userXPData.loads, cargo),
      meters: _add(DataT.userXPData.meters, meter),
    },
    levelData: {
      ...DataT.levelData,
      xp: newXP,
      level: newLevel,
      next: _calcNext(newLevel, DataT.levelData.rank),
    },
  };
};
