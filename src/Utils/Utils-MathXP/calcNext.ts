// import -------------------

import * as config from "../../config.json";

// export -------------------

export const _calcNext = (level: number, rank: number): number =>
  level === config.XP_DATA.XP_MaxLevel
    ? config.XP_DATA.XP_MaxLevel * (config.XP_DATA.XP_CAP * (rank + 1))
    : (level + 1) * (config.XP_DATA.XP_CAP * (rank + 1));
