// import -----------------------------

import * as config from "../../config.json";

// export -----------------------------

export const _calcPrestigePrice = (rank: number): number =>
  config.Money.Price_prestige + config.Money.Reward_next * rank;
