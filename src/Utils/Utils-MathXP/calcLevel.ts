// import ---------------------------

import { pipe } from "fp-ts/lib/function";
import * as config from "../../config.json";

// export ---------------------------

export const _calcLevel = (totxp: number, rank: number): number =>
  pipe(
    Math.floor(totxp / ((rank + 1) * config.XP_DATA.XP_CAP)),
    (level: number) =>
      level >= config.XP_DATA.XP_MaxLevel ? config.XP_DATA.XP_MaxLevel : level
  );
