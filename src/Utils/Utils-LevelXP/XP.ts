// import --------------------------

import * as config from "../../config.json";
import { Context } from "../../Config/Contex";
import { levelAttribute } from "../../DB/Models/levelsTable";
import { _add, _rem } from "../Utils-MathXP/simpleCalc";
import { errorBOT } from "../../Interface/Error";
import { _calcLevel } from "../Utils-MathXP/calcLevel";
import { _calcNext } from "../Utils-MathXP/calcNext";
import { pipe } from "fp-ts/lib/function";
import * as TE from "fp-ts/lib/TaskEither";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _addRoles } from "./addRoles";
import { _remRoles } from "./remRoles";
import { _addMoney } from "../Utils-User/addMoney";
import { _calcAddMoney, _calcRemMoney } from "../Utils-MathXP/calcMoney";
import { _remMoney } from "../Utils-User/remMoney";

// function --------------------------

const _calcAddXP = (xp: number, attribute: levelAttribute): levelAttribute => {
  // if (attribute.banned === true) return attribute; // Check if user is banned
  const newXP: number = _add(attribute.xp, xp);
  const newLevel: number = _calcLevel(newXP, attribute.rank);
  const newNEXT: number = _calcNext(newLevel, attribute.rank);
  // console.log(xp);
  return {
    ...attribute,
    xp: newXP,
    level: newLevel,
    next: newNEXT,
  };
};

// export --------------------------

export const _randomXP = (): number => {
  const num = Math.floor(
    Math.random() *
      (config.XP_DATA.XP_Job.Job_Max - config.XP_DATA.XP_Job.Job_Min) +
      config.XP_DATA.XP_Job.Job_Min
  );
  //console.log(`XP Base ${num}`);
  return num;
};

export const _rankXPreset = (xp: number, attribute: levelAttribute) =>
  pipe(
    RTE.of(_calcAddXP(-Math.abs(xp), attribute)),
    RTE.chain((newattribute: levelAttribute) => _remRoles(newattribute))
    // RTE.chainFirstIOK((a) => () => console.log(a, 1111))
  );

export const _remXP = (xp: number, attribute: levelAttribute) =>
  pipe(
    RTE.of(_calcAddXP(-Math.abs(xp), attribute)),
    RTE.chainFirst((newattribute: levelAttribute) =>
      pipe(
        RTE.of(_calcRemMoney(attribute, newattribute)),
        RTE.chain((money: number) => _remMoney(attribute.id, money))
      )
    ),
    RTE.chain((newattribute: levelAttribute) => _remRoles(newattribute))
  );

export const _addXP = (xp: number, attribute: levelAttribute) =>
  pipe(
    RTE.of(_calcAddXP(xp, attribute)),
    // RTE.chainFirstIOK((a) => () => console.log(a)),
    RTE.chainFirst((newattribute: levelAttribute) =>
      pipe(
        RTE.of(_calcAddMoney(attribute, newattribute)),
        RTE.chain((money: number) => _addMoney(attribute.id, money))
      )
    ),
    RTE.chain((newattribute: levelAttribute) => _addRoles(newattribute))
    // RTE.chainFirstIOK((attribute) => () => console.log(attribute))
  );
