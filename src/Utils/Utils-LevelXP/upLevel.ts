// import ----------------------------

import { pipe } from "fp-ts/lib/function";
import { levelAttribute } from "../../DB/Models/levelsTable";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../Config/Contex";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../../Interface/Error";
import { _upDBDataErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export ----------------------------

export const _queryUPLevel = (attribute: levelAttribute) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.update(
              {
                xp: attribute.xp,
                rank: attribute.rank,
                voiceCount: attribute.voiceCount,
                level: attribute.level,
                roles: attribute.roles,
                msgCount: attribute.msgCount,
                next: attribute.next,
                msgRun: attribute.msgRun,
                sync: attribute.sync,
                banned: attribute.banned,
              },
              { where: { id: attribute.id }, transaction: t }
            );
            return data;
          });
          return result;
        },
        (err): errorBOT => _upDBDataErrorBOT(attribute.id, err)
      )
    ),
    RTE.map(() => {
      //console.log(attribute);
      return attribute;
    })
  );
