// import ----------------------------------

import { levelAttribute } from "../../DB/Models/levelsTable";
import { _addXP, _randomXP } from "./XP";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _calcLevel } from "../Utils-MathXP/calcLevel";
import { _calcNext } from "../Utils-MathXP/calcNext";
import { _queryUPLevel } from "./upLevel";
import * as TE from "fp-ts/lib/TaskEither";
import { _checkRoleMulti } from "../Utils-CheckThings/checkRoleMulti";
import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import {
  _mathErrorBOT,
  _upDBDataErrorBOT,
} from "../Utils-Err&Log/listErrorBOT";
import { _addMoney } from "../Utils-User/addMoney";
import { _add } from "../Utils-MathXP/simpleCalc";
import { Message } from "discord.js";
import { _getUserLevel } from "./getLevel";

// function --------------------------------

const _upMsgRun = (attribute: levelAttribute) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.update(
              { msgRun: true },
              { where: { id: attribute.id }, transaction: t }
            );
            return data;
          });
          return result;
        },
        (err): errorBOT => _upDBDataErrorBOT(attribute.id, err)
      )
    ),
    RTE.map(() => attribute)
  );

// export ----------------------------------

export const _remMgs = (attribute: levelAttribute) => {
  return pipe(
    RTE.ask<Context>(),
    RTE.chain((context) =>
      pipe(
        _checkRoleMulti(attribute.id),
        RTE.map(
          (test: boolean): number =>
            _randomXP() *
            (context.mult + (test ? 1 : 0) + (attribute.multRun ? 1 : 0))
        )
      )
    ),
    RTE.chain<Context, errorBOT, number | undefined, number>(
      RTE.fromPredicate(
        (newXP): newXP is number => typeof newXP !== "undefined",
        (err): errorBOT => _mathErrorBOT(attribute.id, err)
      )
    ),
    RTE.chainFirst((xp: number) => _addMoney(attribute.id, xp)),
    RTE.chain((xp: number) => _addXP(xp, attribute)),
    RTE.chain((attribute) =>
      _queryUPLevel({
        ...attribute,
        msgRun: false,
        msgCount: _add(attribute.msgCount, 1),
      })
    )
  );
};

export const _setMgs = (mgs: Message) =>
  pipe(
    RTE.of(mgs.author.id),
    RTE.chain(_getUserLevel),
    RTE.chain((attribute: levelAttribute) => _upMsgRun(attribute))
  );
