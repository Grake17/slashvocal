// import -----------------------------

import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { errorBOT } from "../../Interface/Error";
import { Context } from "../../Config/Contex";
import { levelAttribute } from "../../DB/Models/levelsTable";
import { pipe } from "fp-ts/lib/function";
import * as TE from "fp-ts/lib/TaskEither";
import { _upDBDataErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export ------------------------------

export const _setMult = (attribute: levelAttribute) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.update(
              { multRun: attribute.multRun, multTO: attribute.multTO },
              { where: { id: attribute.id }, transaction: t }
            );
            return data;
          });
          return result;
        },
        (err: unknown): errorBOT => _upDBDataErrorBOT(attribute.id, err)
      )
    ),
    RTE.map(() => attribute)
  );
