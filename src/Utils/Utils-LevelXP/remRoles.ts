// import -------------------------------

import * as config from "../../config.json";
import { Context } from "../../Config/Contex";
import { levelAttribute } from "../../DB/Models/levelsTable";
import { pipe } from "fp-ts/lib/function";
import { Guild, GuildMember, Role } from "discord.js";
import * as A from "fp-ts/lib/Array";
import * as TE from "fp-ts/lib/TaskEither";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _getDiscordGuild } from "../Utils-DiscordAPI/guildDiscordAPI";
import { _getDiscordUser } from "../Utils-DiscordAPI/usersDiscordAPI";
import {
  _addDiscordRole,
  _getDiscordRole,
  _remDiscordRole,
} from "../Utils-DiscordAPI/rolesDiscordAPI";

// function ------------------------------------

const _remRole = (member: GuildMember, roleID: string) =>
  pipe(
    RTE.ask<Guild>(),
    RTE.chainTaskEitherK((guild: Guild) => _getDiscordRole(guild, roleID)),
    RTE.chainTaskEitherK((role: Role) => _remDiscordRole(member, role)),
    RTE.map(() => roleID)
  );

const _filterArr = (attribute: levelAttribute) =>
  pipe(
    config.XP_DATA.XP_Roles,
    A.filter((arr) => attribute.level < arr.LEVEL_Role)
  );

const _manageRoles = (attribute: levelAttribute) =>
  pipe(
    RTE.Do,
    RTE.bind("guild", () => RTE.ask<Guild>()),
    RTE.bind("member", (guild) =>
      RTE.fromTaskEither(_getDiscordUser(guild.guild, attribute.id))
    ),
    RTE.chainTaskEitherK(({ guild, member }) =>
      pipe(
        _filterArr(attribute),
        TE.traverseArray((arrRole) =>
          pipe(
            _remRole(member, arrRole.ID_Role)(guild),
            TE.map((roleID) => {
              attribute.roles = attribute.roles.filter(
                (arr) => !arr.includes(roleID)
              );
            })
          )
        )
      )
    )
  );

// export --------------------------------------

export const _remRoles = (attribute: levelAttribute) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      _getDiscordGuild(context, context.env.GUILD_ID)
    ),
    RTE.chainTaskEitherK((guild: Guild) => _manageRoles(attribute)(guild)),
    RTE.map(() => attribute)
  );
