// import ---------------------------

import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { _upDBDataErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export ---------------------------

export const _setVoice = (id: string) => {
  return pipe(
    RTE.Do,
    RTE.bind("context", () => RTE.ask<Context>()),
    RTE.chainTaskEitherK(({ context }) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.update(
              { voiceRun: true },
              { where: { id: id }, transaction: t }
            );
            return data;
          });
          return result;
        },
        (err): errorBOT => _upDBDataErrorBOT(id, err)
      )
    )
  );
};

export const _remVoice = (id: string) => {
  return pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.update(
              { voiceRun: false },
              { where: { id: id }, transaction: t }
            );
            return data;
          });
          return result;
        },
        (err): errorBOT => _upDBDataErrorBOT(id, err)
      )
    ),
    RTE.map(() => id)
  );
};
