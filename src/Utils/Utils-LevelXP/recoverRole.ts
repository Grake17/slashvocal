// import -------------------------------------

import { Guild, GuildMember, Role } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/ReaderTaskEither";
import { Context } from "../../Config/Contex";
import { levelAttribute } from "../../DB/Models/levelsTable";
import {
  _addDiscordRole,
  _getDiscordRole,
} from "../Utils-DiscordAPI/rolesDiscordAPI";
import * as A from "fp-ts/Array";
import { _getUserLevel } from "./getLevel";
import * as config from "../../config.json";
import * as TE from "fp-ts/lib/TaskEither";
import { _getDiscordUser } from "../Utils-DiscordAPI/usersDiscordAPI";
import { _getDiscordGuild } from "../Utils-DiscordAPI/guildDiscordAPI";

// function -------------------------

const _addRole = (member: GuildMember, roleID: string) =>
  pipe(
    RTE.ask<Guild>(),
    RTE.chainTaskEitherK((guild: Guild) => _getDiscordRole(guild, roleID)),
    RTE.chainTaskEitherK((role: Role) => _addDiscordRole(member, role)),
    RTE.map(() => roleID)
  );

const _filterArr = (attribute: levelAttribute) =>
  pipe(
    config.XP_DATA.Lock_Roles,
    (arr) => arr.concat(config.XP_DATA.XP_Roles),
    A.filter(
      (arr) =>
        attribute.level >= arr.LEVEL_Role
    )
  );

  const _manageRoles = (attribute: levelAttribute) =>
  pipe(
    RTE.Do,
    RTE.bind("guild", () => RTE.ask<Guild>()),
    RTE.bind("member", (guild) =>
      RTE.fromTaskEither(_getDiscordUser(guild.guild, attribute.id))
    ),
    RTE.chainTaskEitherK(({ guild, member }) =>
      pipe(
        _filterArr(attribute),
        TE.traverseArray((arrRole) =>
          pipe(
            _addRole(member, arrRole.ID_Role)(guild),
            TE.map((roleID) => {
              attribute.roles = attribute.roles.concat(roleID);
            })
          )
        )
      )
    )
  );

// export --------------------------------------

const _addRoles = (attribute: levelAttribute) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      _getDiscordGuild(context, context.env.GUILD_ID)
    ),
    //RTE.chainFirstIOK(() => () => console.log(attribute)),
    RTE.chainTaskEitherK((guild: Guild) => _manageRoles(attribute)(guild)),
    RTE.map(() => attribute)
  );

// export ----------------------------

export const _recoverRole = (userId: string) =>
  pipe(
    RTE.Do,
    RTE.bind("ctx", () => RTE.ask<Context>()),
    RTE.bind("attribute", () => _getUserLevel(userId)),
    RTE.chainTaskEitherK(({ ctx, attribute }) => _addRoles(attribute)(ctx))
  );
