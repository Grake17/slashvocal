// import ------------------------------------

import { levelAttribute } from "../../DB/Models/levelsTable";
import { Context } from "../../Config/Contex";
import { errorBOT } from "../../Interface/Error";
import { pipe } from "fp-ts/lib/function";
import * as TE from "fp-ts/lib/TaskEither";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import {
  _getDBDataErrorBOT,
  _userRegisterErrotBOT,
} from "../Utils-Err&Log/listErrorBOT";

// export ----------------------------------

export const _getUserLevel = (
  id: string
): RTE.ReaderTaskEither<Context, errorBOT, levelAttribute> => {
  return pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.findOne({
              where: { id: id },
              transaction: t,
            });
            return data;
          });
          return result;
        },
        (err): errorBOT => _getDBDataErrorBOT(id, err)
      )
    ),
    RTE.map((data) => data?.get()),
    RTE.chain<Context, errorBOT, levelAttribute | undefined, levelAttribute>(
      RTE.fromPredicate(
        (a): a is levelAttribute => typeof a !== "undefined",
        (err): errorBOT => _userRegisterErrotBOT(id, err)
      )
    )
  );
};
