// import -------------------------------

import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../Config/Contex";
import * as config from "../../config.json";
import { errorBOT } from "../../Interface/Error";
import { User } from "discord.js";
import { _permissionErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export --------------------------------

export const _cmdPermission = () =>
  RTE.chain<Context, errorBOT, User, User>(
    RTE.fromPredicate(
      (user) => Boolean(config.Permission.find((obj) => obj.ID === user.id)),
      (user): errorBOT => _permissionErrorBOT(user)
    )
  );

export const _checkPermission = (id: string): boolean =>
  Boolean(config.Permission.find((obj) => obj.ID === id));
