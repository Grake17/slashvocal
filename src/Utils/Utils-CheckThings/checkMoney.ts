// import ----------------------------

import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../Config/Contex";
import { userAttribute } from "../../DB/Models/usersTable";
import { errorBOT } from "../../Interface/Error";
import { _moneyErrorBot, _syntaxErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export ----------------------------

export const _checkMoney = (n: number) =>
  RTE.chain<Context, errorBOT, userAttribute, userAttribute>(
    RTE.fromPredicate(
      (attribute: userAttribute) => attribute.saldo >= n,
      (attribute) =>
        _moneyErrorBot(attribute.userId)
    )
  );
