// import -------------------------

import { pipe } from "fp-ts/lib/function";
import { Context } from "../../Config/Contex";
import * as config from "../../config.json";
import { Guild, GuildMember, Role } from "discord.js";
import * as TE from "fp-ts/lib/TaskEither";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { errorBOT } from "../../Interface/Error";
import { _getDiscordUser } from "../Utils-DiscordAPI/usersDiscordAPI";
import { _getDiscordGuild } from "../Utils-DiscordAPI/guildDiscordAPI";
import {
  _getDiscordRole,
  _hasDiscordRole,
} from "../Utils-DiscordAPI/rolesDiscordAPI";
import { boolean } from "io-ts";

// export -------------------------

// export const _checkRoleMulti2 = (
//   id: string
// ): RTE.ReaderTaskEither<Context, errorBOT, boolean> =>
//   pipe(
//     RTE.ask<Context>(),
//     RTE.chainTaskEitherK((context: Context) =>
//       pipe(
//         _getDiscordGuild(context, context.env.GUILD_ID),
//         TE.chain((guild: Guild) => _getDiscordUser(guild, id)),
//         TE.map((member: GuildMember) =>
//           _hasDiscordRole(member, config.XP_DATA.XP_RoleDouble)
//         ),
//         TE.chainFirstIOK((a) => () => console.log(a))
//       )
//     )
//   );

export const _checkRoleMulti = (id: string) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      pipe(
        _getDiscordGuild(context, context.env.GUILD_ID),
        TE.chain((guild: Guild) =>
          _getDiscordRole(guild, config.XP_DATA.XP_RoleDouble)
        ),
        TE.map((role: Role) =>
          role.members.map((member: GuildMember) => member.id === id)
        ),
        // TE.chainFirstIOK((a) => () => console.log(a)),
        TE.map((a) => a.includes(true)),
        // TE.chainFirstIOK((a) => () => console.log(a))
      )
    )
  );
