// import ---------------------------

import { Message, User } from "discord.js";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "vm";
import { errorBOT } from "../../Interface/Error";
import { _syntaxErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export ---------------------------

export const _checkAssignXP = (msg: Message) =>
  RTE.chain<Context, errorBOT, User | undefined, User>(
    RTE.fromPredicate(
      (user): user is User =>
        !isNaN(Number(msg.content.split(" ")[1])) &&
        typeof user !== "undefined",
      (user): errorBOT =>
        _syntaxErrorBOT(
          `Utente: ${user}\nNum XP: ${Number(msg.content.split(" ")[1])}`
        )
    )
  );
