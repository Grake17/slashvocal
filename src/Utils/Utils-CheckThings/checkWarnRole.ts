// import --------------------------

import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { Context } from "../../Config/Contex";
import { _getDiscordGuild } from "../Utils-DiscordAPI/guildDiscordAPI";
import { _getDiscordRole } from "../Utils-DiscordAPI/rolesDiscordAPI";
import { _getDiscordUser } from "../Utils-DiscordAPI/usersDiscordAPI";
import * as config from "../../config.json";

// export ------------------------

export const _checkWarnRole = (id: string) =>
  pipe(
    RTE.Do,
    RTE.bind("context", () => RTE.ask<Context>()),
    RTE.bind("guild", ({ context }) =>
      RTE.fromTaskEither(_getDiscordGuild(context, context.env.GUILD_ID))
    ),
    RTE.chainTaskEitherK(({ context, guild }) =>
      pipe(
        _getDiscordRole(guild, config.XP_DATA.XP_RoleBlock),
        TE.map((role) =>
          Boolean(role.members.find((member) => member.id === id))
        )
      )
    )
  );
