// import -------------------------

import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../Config/Contex";
import { levelAttribute } from "../../DB/Models/levelsTable";
import { errorBOT } from "../../Interface/Error";
import { _banErrorBOT } from "../Utils-Err&Log/listErrorBOT";

// export -------------------------

export const _checkUserBan = () =>
  RTE.chain<Context, errorBOT, levelAttribute, levelAttribute>(
    RTE.fromPredicate(
      (attribute) => attribute.banned !== true,
      (attribute): errorBOT => _banErrorBOT(attribute.id)
    )
  );
