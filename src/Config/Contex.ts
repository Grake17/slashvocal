// import -----------------------

import { Sequelize } from "sequelize";
import { Client } from "discord.js";
import { envType } from "./Env";
import { Tables } from "../DB/interfaceTable";

// exports ----------------------

export type Context = {
  client: Client;
  env: envType;
  sequelize: Sequelize;
  tables: Tables;
  mult: number;
  prefix: string;
};
