// import -------------------------

import * as t from "io-ts";

// export -------------------------

export const envC = t.type(
  {
    TOKEN: t.string,
    GUILD_ID: t.string,
    URL_REG: t.string,
    DATABASE_HOST: t.string,
    DATABASE_NAME: t.string,
    DATABASE_USER: t.string,
    DATABASE_PASSWORD: t.string,
    TITLE_ID: t.string,
    API_KEY: t.string,
    API_KEY_XBL: t.string,
  },
  "Environment"
);

// type ----------------------------

export type envType = t.TypeOf<typeof envC>;
