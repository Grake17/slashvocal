// import ------------------------

import { envType } from "../Config/Env";
import { Model, ModelCtor, Sequelize } from "sequelize";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { error } from "fp-ts/lib/Console";

// tables ------------------------

import { userAttribute, userTable } from "./Models/usersTable";
import { userXPAttribute, userXPTable } from "./Models/userXPsTable";
import { levelAttribute, levelTable } from "./Models/levelsTable";
import { Tables } from "../DB/interfaceTable";
import { oldxpAttribute, oldxpTable } from "./Models/oldXP";

// funcition ----------------------

const auth = (sequelize: Sequelize) =>
  TE.tryCatch(
    () =>
      sequelize
        .authenticate()
        .then(() => console.log("[DB] Connection success!")),
    () => error(`Error on DB connection!`)
  );

// export --------------------------

export const sequelize = (env: envType) => {
  return new Sequelize({
    host: env.DATABASE_HOST,
    database: env.DATABASE_NAME,
    username: env.DATABASE_USER,
    password: env.DATABASE_PASSWORD,
    dialect: "postgres",
    logging: false,
    define: {
      timestamps: false,
    },
    pool: {
      idle: 10000,
      evict: 20000,
    },
  });
};

export const dbObj = () => {
  return pipe(
    RTE.ask<envType>(),
    RTE.bindTo("env"),
    RTE.bind("sequelize", ({ env }) => RTE.of(sequelize(env))),
    RTE.chainFirst(({ sequelize }) => RTE.fromTaskEither(auth(sequelize))),
    RTE.map(({ sequelize }) => {
      // Define Tables
      const level: ModelCtor<Model<levelAttribute>> = sequelize.define(
        levelTable.name,
        levelTable.model
      );
      const userXP: ModelCtor<Model<userXPAttribute>> = sequelize.define(
        userXPTable.name,
        userXPTable.model
      );
      const user: ModelCtor<Model<userAttribute>> = sequelize.define(
        userTable.name,
        userTable.model
      );
      const oldxp: ModelCtor<Model<oldxpAttribute>> = sequelize.define(
        oldxpTable.name,
        oldxpTable.model
      );
      const tables: Tables = {
        user: user,
        level: level,
        userXP: userXP,
        oldxp: oldxp,
      };
      return { tables, sequelize };
    })
  );
};
