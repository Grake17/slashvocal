// import ---------------------------------

import { Model, ModelCtor } from "sequelize";
import { levelAttribute } from "./Models/levelsTable";
import { oldxpAttribute } from "./Models/oldXP";
import { userAttribute } from "./Models/usersTable";
import { userXPAttribute } from "./Models/userXPsTable"; 

// export -------------------------------

export interface Tables {
  level: ModelCtor<Model<levelAttribute>>;
  userXP: ModelCtor<Model<userXPAttribute>>;
  user: ModelCtor<Model<userAttribute>>;
  oldxp: ModelCtor<Model<oldxpAttribute>>;
}
