// Import ----------------------------

import { boolean } from "io-ts";
import { ARRAY, BOOLEAN, NUMBER, STRING } from "sequelize";
import * as config from "../../config.json";

// table -----------------------------

const levelTable = {
  name: "levels",
  model: {
    id: {
      type: STRING,
      unique: true,
      primaryKey: true,
    },
    xp: {
      type: NUMBER,
      defaultValue: 0,
    },
    level: {
      type: NUMBER,
      defaultValue: 0,
    },
    next: {
      type: NUMBER,
      defaultValue: config.XP_DATA.XP_CAP,
    },
    rank: {
      type: NUMBER,
      defaultValue: 0,
    },
    voiceRun: {
      type: BOOLEAN,
      defultValue: false,
    },
    voiceCount: {
      type: NUMBER,
      defaultValue: 0,
    },
    msgRun: {
      type: BOOLEAN,
      defultValue: false,
    },
    msgCount: {
      type: NUMBER,
      defaultValue: 0,
    },
    roles: {
      type: ARRAY(STRING),
      defaultValue: null,
    },
    banned: {
      type: BOOLEAN,
      defaultValue: false,
    },
    multRun: {
      type: BOOLEAN,
      defaultValue: false,
    },
    multTO: {
      type: NUMBER,
      defaultValue: 0,
    },
    sync: {
      type: BOOLEAN,
      defaultValue: false
    }
  },
};

// Interface --------------------------

interface levelAttribute {
  id: string;
  xp: number;
  level: number;
  next: number;
  rank: number;
  voiceRun?: boolean;
  voiceCount: number;
  msgRun?: boolean;
  msgCount: number;
  roles: string[];
  banned: boolean;
  multRun: boolean;
  multTO: number;
  sync: boolean;
}

// Export ------------------------------

export { levelAttribute, levelTable };
