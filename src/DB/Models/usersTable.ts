// import --------------------------------

import { NUMBER, STRING } from "sequelize";

// Table ---------------------------------

const userTable = {
  name: "users",
  model: {
    userId: {
      type: STRING,
      unique: true,
      primaryKey: true,
    },
    saldo: {
      type: NUMBER,
    },
  },
};

// interface ------------------------------

interface userAttribute {
  userId: string;
  saldo: number;
}

// export ---------------------------------

export { userAttribute, userTable };
