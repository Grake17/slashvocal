// import ----------------------------

import { NUMBER, STRING } from "sequelize";

// table ----------------------------

const oldxpTable = {
  name: "userExps",
  model: {
    userId: {
      type: STRING,
      unique: true,
      primaryKey: true,
    },
    xp: {
      type: NUMBER,
      defaultValue: 0,
    },
  },
};

// interface -----------------------

interface oldxpAttribute {
  userId: string;
  xp: number;
}

// export ---------------------------

export { oldxpTable, oldxpAttribute };
