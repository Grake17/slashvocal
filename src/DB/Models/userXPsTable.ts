// import -------------------------------

import { NUMBER, STRING } from "sequelize";

// table --------------------------------

export const userXPTable = {
  name: 'userXPs',
  model: {
    id: {
      type: STRING,
      unique: true,
      primaryKey: true,
    },
    xuid: {
      type: STRING,
      unique: true,
    },
    gt: STRING,
    tresures: {
      type: NUMBER,
      defaultValue: 0,
    },
    skulls: {
      type: NUMBER,
      defaultValue: 0,
    },
    loads: {
      type: NUMBER,
      defaultValue: 0,
    },
    voyages: {
      type: NUMBER,
      defaultValue: 0,
    },
    isles: {
      type: NUMBER,
      defaultValue: 0,
    },
    meters: {
      type: NUMBER,
      defaultValue: 0,
    },
    achievement: {
      type: NUMBER,
      defaultValue: 0,
    },
  },
};

// interface -------------------------

export interface userXPAttribute {
  id: string;
  xuid: string;
  gt: string;
  tresures: number;
  skulls: number;
  loads: number;
  voyages: number;
  isles: number;
  meters: number;
  achievement: number;
}
