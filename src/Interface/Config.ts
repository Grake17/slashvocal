// export ----------------------------

export interface IntConfig {
  BOT_Data: {
    author: string;
    Prefix: string;
    LOG_Channel: {
      LOG_ID: string;
      LOG_Colors: string[];
    };
    CMD_Channel: {
      CMD_ID: string[];
      CMD_Color: string;
    };
  };
  Money: {
    Price_Mult: number;
    Price_prestige: number;
    Reward_next: number;
    Rank_mult: number;
    Price_rankNext: number;
  };
  XP_DATA: {
    XP_Job: {
      Job_Max: number;
      Job_Min: number;
      XP_Mult: number;
    };
    XP_RoleDouble: string;
    XP_MaxLevel: number;
    XP_Minlevel: number;
    XP_CAP: number;
    XP_RewardGame: {
      XP_Medal: number;
      XP_Achivement: number;
      XP_Vojage: number;
      XP_Items: number;
      XP_Mile: number;
      XP_Island: number;
    };
    Lock_Roles: {
      ID_Role: string;
      NAME_Role: string;
      LEVEL_Role: number;
      Reward_Role: number;
    }[];
    XP_Roles: {
      ID_Role: string;
      NAME_Role: string;
      LEVEL_Role: number;
      Reward_Role: number;
    }[];
  };
  Permission: { name: string; ID: string }[];
  Medal: { id: string; name: string; path: string }[];
}
