// import ----------------------

import { levelAttribute } from "../DB/Models/levelsTable";
import { userXPAttribute } from "../DB/Models/userXPsTable";

// export ----------------------

export type resultObj = {
  levelData: levelAttribute | undefined;
  userXPData: userXPAttribute | undefined;
};

export type diffObj = {
  oldValidateObj: validateObj;
  newValidateObj: validateObj;
};

export type validateObj = {
  levelData: levelAttribute;
  userXPData: userXPAttribute;
};
