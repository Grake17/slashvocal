// import ------------------------------

import { Message } from "discord.js";
import { Context } from "../Config/Contex";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { errorBOT } from "./Error";

// export -----------------------------

export interface Command {
  name: string;
  description: string;
  syntax: string;
  admin: boolean;
  execute: (
    mgs: Message,
    context: Context
  ) => RTE.ReaderTaskEither<Context, errorBOT, Message>;
}
