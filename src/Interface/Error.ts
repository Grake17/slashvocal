// export ---------------------------------

export interface errorBOT {
  name: string;
  description: string;
  rank: number
  errCode: unknown;
}
