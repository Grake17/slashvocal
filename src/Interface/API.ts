// export -------------------------------

// Interface Achivement API
export type AchivementT = {
  achievements: [
    {
      id: string;
      name: string;
      progressState: string;
      mediaAssets: [
        {
          name: string;
          type: string;
          url: string;
        }
      ];
    }
  ];
};

// Interface Stats API
export type StatsObj = {
  name: string;
  value: number;
}[];

export type StatsT = {
  groups: [
    {
      statlistscollection: [
        {
          stats: [{ name: string; value: number }];
        }
      ];
    }
  ];
};
