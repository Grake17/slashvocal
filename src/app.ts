// import ---------------------------

import { Context } from "./Config/Contex";
import { envType, envC } from "./Config/Env";
import * as dotenv from "dotenv";
import { dbObj } from "./DB/db";
import { _msgHandler } from "./handlers/mgsHandler";
import { _voiceHandler } from "./handlers/voiceHandler";
import { Client } from "discord.js";
import { constVoid, flow, pipe } from "fp-ts/lib/function";
import { error } from "fp-ts/lib/Console";
import * as E from "fp-ts/lib/Either";
import * as IOE from "fp-ts/lib/IOEither";
import { _setMgs } from "./Utils/Utils-LevelXP/setMgs";
import * as config from "./config.json";
import { _multJob } from "./Job/multJob";
import { _MsgVoiceJob } from "./Job/Msg&Voice";
import { _recoverRole } from "./Utils/Utils-LevelXP/recoverRole";

// function ----------------------------

const start = (env: envType) => async () => {
  const db = await dbObj()(env)();
  pipe(
    db,
    E.map((a) => {
      const client = new Client();
      const context: Context = {
        client: client,
        env: env,
        sequelize: a.sequelize,
        tables: a.tables,
        mult: config.XP_DATA.XP_Job.XP_Mult,
        prefix: config.BOT_Data.Prefix,
      };
      // Discord Handler
      client.on("ready", () => {
        client.user?.setActivity({
          type: "LISTENING",
          name: `${config.BOT_Data.Prefix}help`,
        });
        console.log(`[Discord] ${client.user?.username}: ready!`);
      });
      // Msg Handler
      client.on("message", (msg) => {
        if(msg.author.bot == true) return;
        _msgHandler(msg, context)(context)();
        _setMgs(msg)(context)();
      });
      // Voice Handler
      client.on("voiceStateUpdate", (oldMember, newMember) =>
        _voiceHandler(oldMember, newMember, context)
      );
      // Join server Handler
      client.on("guildMemberAdd", async (guildMember) => {
        _recoverRole(guildMember.user.id)(context)();
      });
      // Start Job
      _MsgVoiceJob(context).start();
      _multJob(context).start();

      // Login
      client.login(env.TOKEN);
    }),
    E.mapLeft((e) => e())
  );
};

// pipe start ---------------------------

pipe(
  dotenv.config().parsed,
  envC.decode,
  IOE.fromEither,
  IOE.chain(flow(start, IOE.fromIO)),
  IOE.fold(error, () => constVoid)
)();
