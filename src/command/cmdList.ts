// import ---------------------------

import { Command } from "../Interface/Command";

// commands -------------------------

import { _add_xp } from "./commands/CMD_Admin/addxp";
import { _ban_cmd } from "./commands/CMD_Admin/banxp";
import { _servermult } from "./commands/CMD_Admin/servermult";
import { _rem_cmd } from "./commands/CMD_Admin/remxp";
import { _unban_cmd } from "./commands/CMD_Admin/unbanxp";
import { _incassa } from "./commands/CMD_User/incassa";
import { _listapremi } from "./commands/CMD_User/listapremi";
import { _ping } from "./commands/CMD_User/ping";
import { _prestigio } from "./commands/CMD_User/prestigio";
import { _register } from "./commands/CMD_User/register";
import { _statistiche } from "./commands/CMD_User/statistiche";
import { _level } from "./commands/level";
import { _status } from "./commands/CMD_User/status";
import { _claimmensile } from "./commands/CMD_User/claimmensile";
import { _syncxp } from "./commands/CMD_User/syncxp";
import { _help } from "./commands/CMD_User/help";
import { _claimmult } from "./commands/CMD_User/claimmult";

// export ---------------------------

export const cmds: Command[] = [
  // _incassa,
  _register,
  // _statistiche,
  _prestigio,
  _level,
  _ping,
  _add_xp,
  _rem_cmd,  
  _unban_cmd,
  _ban_cmd,
  _listapremi,
  _servermult,
  _status,
  _claimmensile,
  _syncxp,
  _help,
  _claimmult
];
