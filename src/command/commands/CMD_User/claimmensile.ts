// import -------------------------------

import { Message, MessageEmbed } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../../Config/Contex";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import * as config from "../../../config.json";
import { Command } from "../../../Interface/Command";
import { _checkMoney } from "../../../Utils/Utils-CheckThings/checkMoney";
import { _checkUserBan } from "../../../Utils/Utils-CheckThings/checkUserBan";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import { _queryUPLevel } from "../../../Utils/Utils-LevelXP/upLevel";
import { _getMoney } from "../../../Utils/Utils-User/getMoney";
import { _payMoney } from "../../../Utils/Utils-User/payMoney";
import { _remMoney } from "../../../Utils/Utils-User/remMoney";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _setMult } from "../../../Utils/Utils-LevelXP/setMult";
import { _descriptionClaimmult, _descriptionClaimmultMensile } from "../../../Doc/cmdDescription";
import { errorBOT } from "../../../Interface/Error";
import {
  _multClaimedErrorBot,
  _multErrorBOT,
} from "../../../Utils/Utils-Err&Log/listErrorBOT";

// function ------------------------------

const _calcDate = (attribute: levelAttribute) =>
  Number(attribute.multTO) === 0
    ? Date.now() + 24 * 30 * 60 * 60 * 1000
    : Number(attribute.multTO) + 24 * 60 * 60 * 1000;

// export --------------------------------

export const _claimmensile: Command = {
  name: "claimmensile",
  syntax: `${config.BOT_Data.Prefix}claimmensile`,
  admin: false,
  description: _descriptionClaimmultMensile,
  execute(msg: Message, context: Context) {
    return pipe(
      _getUserLevel(msg.author.id),
      _checkUserBan(),
      RTE.chain<Context, errorBOT, levelAttribute, levelAttribute>(
        RTE.fromPredicate(
          (attribute) => attribute.multRun !== true,
          (attribute) => _multClaimedErrorBot(attribute)
        )
      ),
      RTE.chainFirst((attribute: levelAttribute) =>
        _payMoney(attribute.id, config.Money.Price_Mult_Mensile)
      ),
      RTE.map((attribute: levelAttribute) => {
        return {
          ...attribute,
          multTO: _calcDate(attribute),
          multRun: true,
        };
      }),
      RTE.chain((attribute: levelAttribute) => _setMult(attribute)),
      RTE.map((attribute: levelAttribute) => {
        const embed = new MessageEmbed()
          .setAuthor(config.BOT_Data.author)
          .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
          .setDescription(
            `Congratulazioni ${
              msg.author
            } moltiplicatore riscattato!\nAvrai un boost fino al giorno ${new Date(
              Number(attribute.multTO)
            ).toLocaleString("en-gb")}`
          );
        return embed;
      }),
      RTE.chainTaskK((embed) => _sendEmbed(msg, embed))
    );
  },
};
