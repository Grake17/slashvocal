// import ------------------------------

import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../../Config/Contex";
import * as config from "../../../config.json";
import { Command } from "../../../Interface/Command";
import { Message, MessageEmbed } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../../../Interface/Error";
import {
  _getDBDataErrorBOT,
  _syncErrorBOT,
} from "../../../Utils/Utils-Err&Log/listErrorBOT";
import { oldxpAttribute } from "../../../DB/Models/oldXP";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import { _addXP } from "../../../Utils/Utils-LevelXP/XP";
import { _queryUPLevel } from "../../../Utils/Utils-LevelXP/upLevel";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _descriptionSyncXP } from "../../../Doc/cmdDescription";

// function ---------------------------

const _getOld = (id: string) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.oldxp.findOne({
              where: { userId: id },
              transaction: t,
            });
            return data;
          });
          return result;
        },
        (err): errorBOT => _getDBDataErrorBOT(id, err)
      )
    ),
    RTE.map((result) => result?.get()),
    RTE.chain<Context, errorBOT, oldxpAttribute | undefined, oldxpAttribute>(
      RTE.fromPredicate(
        (attribute): attribute is oldxpAttribute =>
          typeof attribute !== "undefined",
        () => _syncErrorBOT(id, false, true)
      )
    )
  );

const _sync = (attribute: levelAttribute) =>
  pipe(
    _getOld(attribute.id),
    RTE.chain((oldxp) =>
      pipe(
        _addXP(oldxp.xp, attribute),
        RTE.map((attribute: levelAttribute) => {
          return { ...attribute, sync: true };
        }),
        RTE.chain(_queryUPLevel),
        RTE.map((attribute: levelAttribute) => {
          const text = [
            `**Ahoy <@!${attribute.id}>** la tua esperienza è stata sincronizzata`,
            `XP Aggiunta: **${new Intl.NumberFormat().format(oldxp.xp)}**`,
          ].join("\n");
          const embed = new MessageEmbed()
            .setAuthor(config.BOT_Data.author)
            .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
            .setDescription(text);
          return embed;
        })
      )
    )
  );

// export -----------------------------

export const _syncxp: Command = {
  name: "syncxp",
  description: _descriptionSyncXP,
  syntax: `${config.BOT_Data.Prefix}syncxp`,
  admin: false,
  execute(msg: Message, context: Context) {
    return pipe(
      _getUserLevel(msg.author.id),
      RTE.chain<Context, errorBOT, levelAttribute, levelAttribute>(
        RTE.fromPredicate(
          (attribute) => attribute.sync !== true,
          () => _syncErrorBOT(msg.author.id, true, true)
        )
      ),
      RTE.chain((attribute: levelAttribute) => _sync(attribute)),
      RTE.chainTaskK((embed: MessageEmbed) => _sendEmbed(msg, embed))
    );
  },
};
