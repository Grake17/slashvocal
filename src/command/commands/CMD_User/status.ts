// import --------------------------------

import { Message, MessageEmbed } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../../Config/Contex";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import { Command } from "../../../Interface/Command";
import * as config from "../../../config.json";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import { _checkRoleMulti } from "../../../Utils/Utils-CheckThings/checkRoleMulti";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _descriptionStatus } from "../../../Doc/cmdDescription";

// function ------------------------------

const _takeStatusUser = (
  msg: Message,
  attribute: levelAttribute,
  context: Context
) =>
  pipe(
    _checkRoleMulti(attribute.id),
    RTE.map((check: boolean) => {
      const text = [
        `👤 Profilo Utente: **${msg.author}**`,
        `⏱ Minuti in vocale: **${attribute.voiceCount}**`,
        `📝 Messaggi scritti: **${attribute.msgCount}**`,
        `💵 Potenziamento server: **${check ? "✅" : "❌"}**`,
        `💷 Moltiplicatore personale: **${attribute.multRun ? "✅" : "❌"}**`,
        `📆 Data scadenza moltiplicatore: ${
          attribute.multRun
            ? new Date(Number(attribute.multTO)).toLocaleString("en-gb")
            : "Nessuna"
        }`,
        `----------------------------------------------------`,
        `💻 Server XP/Talleri al mn: ${
          config.XP_DATA.XP_Job.Job_Min *
          (context.mult + (check ? 1 : 0) + (attribute.multRun ? 1 : 0))
        } - ${
          config.XP_DATA.XP_Job.Job_Max *
          (context.mult + (check ? 1 : 0) + (attribute.multRun ? 1 : 0))
        }`,
        `📈 Moltiplicatore xp server: **x${
          context.mult + (check ? 1 : 0) + (attribute.multRun ? 1 : 0)
        }**`,
      ].join("\n");
      const embed = new MessageEmbed()
        .setAuthor(config.BOT_Data.author)
        .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
        .setTitle(`Status XP del server: *${msg.guild?.name}*`)
        .setDescription(text);
      return embed;
    })
  );

// export --------------------------------

export const _status: Command = {
  name: "status",
  description: _descriptionStatus,
  syntax: `${config.BOT_Data.Prefix}status`,
  admin: false,
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.ask<Context>(),
      RTE.chain((context: Context) =>
        pipe(
          _getUserLevel(msg.author.id),
          RTE.chain((attribute: levelAttribute) =>
            _takeStatusUser(msg, attribute, context)
          )
        )
      ),
      RTE.chainTaskK((embed: MessageEmbed) => _sendEmbed(msg, embed))
    );
  },
};
