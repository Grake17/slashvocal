// import ------------------------------------

import { Message, MessageEmbed } from "discord.js";
import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { errorBOT } from "../../../Interface/Error";
import { pipe } from "fp-ts/lib/function";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import * as config from "../../../config.json";
import { _descriptionListaPremi } from "../../../Doc/cmdDescription";

// function ----------------------------------

const _composeRolesText = (level: number) =>
  pipe(
    config.XP_DATA.Lock_Roles,
    (arr) => arr.concat(config.XP_DATA.XP_Roles),
    (arr) =>
      arr
        .filter((role) => role.LEVEL_Role === level)
        .map((role) => `<@&${role.ID_Role}> + **${role.Reward_Role}** Talleri`)
  );

// export ------------------------------------

export const _listapremi: Command = {
  name: "listapremi",
  description: _descriptionListaPremi,
  syntax: `${config.BOT_Data.Prefix}listapremi`,
  admin: false,
  execute(
    msg: Message,
    context: Context
  ): RTE.ReaderTaskEither<Context, errorBOT, Message> {
    return pipe(
      RTE.of(msg),
      RTE.map((msg) => {
        const text = [
          `💰 Ecco la lista dei premi ottenibili con **Cotton2.0!** 💰`,
          `\n⏬ RUOLI ⏬`,
          `I ruoli si ottengo in base al tuo livello su **Discord**!`,
          `Alcuni livelli contrassegnati con 🔸 non si perdono quando si passa al prestigio successivo!`,
          `🔸 Livello **1**: ${_composeRolesText(1)}`,
          `🔸 Livello **5**: ${_composeRolesText(5)}`,
          `🔹 Livello **30**: ${_composeRolesText(30)} `,
          `🔹 Livello **60**: ${_composeRolesText(60)}`,
          `🔹 Livello **80**: ${_composeRolesText(80)}`,
          `🔹 Livello **100**: ${_composeRolesText(100)}`,
          // `\n⏬ Statistiche ⏬`,
          // `Abbiamo stretto un accordo con il *traghettatore* e ora possiamo trasportare i tesori dai mari di **Sea of Thives** direttamente nel nostro server!`,
          // `Tramite il comando **$incassa** si potrà chiedere un traferimento del gruzzoletto che si convertirà in XP e Talleri 🤑`,
          // `⚠️ Il viaggio è **lungo e tortuoso** può andare **MALE**!\n⚠️ In questo caso basterà richiederne un'altro!`,
          // `Ecco quanto si guadagna in base agli incassi!`,
          // `🔸 **Forzieri** incassati => 3 XP/Talleri`,
          // `🔸 **Teschi** incassati => 3 XP/Talleri`,
          // `🔸 **Casse mercante** incassate => 3 XP/Talleri`,
          // `🔸 **Viaggi** completati => 5 XP/Talleri`,
          // `🔸 **Obiettivi** completati => 10 XP/Talleri`,
          // `🔸 **Isole** visitate => 1 XP/Talleri`,
          // `🔸 **Miglia** percorse => 0.01 XP/Talleri`,
        ].join("\n");

        const embed = new MessageEmbed()
          .setAuthor(config.BOT_Data.author)
          .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
          .setDescription(text);
        return embed;
      }),
      RTE.chainTaskK((embed: MessageEmbed) => _sendEmbed(msg, embed))
    );
  },
};
