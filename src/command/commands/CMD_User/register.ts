// import ----------------------------

import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import * as config from "../../../config.json";
import { Message, MessageEmbed } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import * as T from "fp-ts/lib/Task";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _descriptionRegister } from "../../../Doc/cmdDescription";
import { _syntaxErrorBOT } from "../../../Utils/Utils-Err&Log/listErrorBOT";
import { errorBOT } from "../../../Interface/Error";

// function --------------------------

// const _embedPrivate = (msg: Message) => {const text = [
//               `**Ahoy ${msg.author}**, Cotton ti dà il benvenuto!`,
//               `Registrati ora al nuovo sistema di XP: ${context.env.URL_REG}`,
//             ].join("\n");
//             const embed = new MessageEmbed()
//               .setAuthor(config.BOT_Data.author)
//               .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
//               .setDescription(text);
//             return embed;} 

const _deleteMsg =
  (msg: Message): T.Task<Message> =>
  () =>
    msg.delete();

// export ----------------------------

export const _register: Command = {
  name: "register",
  description: _descriptionRegister,
  syntax: `${config.BOT_Data.Prefix}register`,
  admin: false,
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.ask<Context>(),
      RTE.chainTaskEitherK((context: Context) =>
        pipe(
          TE.fromIO(() => {
            const text = [
              `**Ahoy ${msg.author}**, Cotton ti dà il benvenuto!`,
              `Registrati ora al nuovo sistema di XP: ${context.env.URL_REG}`,
            ].join("\n");
            const embed = new MessageEmbed()
              .setAuthor(config.BOT_Data.author)
              .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
              .setDescription(text);
            return embed;
          }),
          TE.chain<errorBOT, MessageEmbed, Message>((embed: MessageEmbed) =>
            TE.tryCatch(
              () => msg.author.send(embed),
              (err) =>
                _syntaxErrorBOT("Impossibile mandare messaggio in privato")
            )
          ),
          TE.chainFirstTaskK(() => _deleteMsg(msg))
        )
      )
    );
  },
};
