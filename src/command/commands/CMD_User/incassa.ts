// import ------------------------------

import { Message, MessageEmbed } from "discord.js";
import { resultObj, validateObj } from "../../../Interface/ObiT";
import { _calcAchivement } from "../../../Utils/Utils-MathXP/calcAchivement";
import { _calcStats } from "../../../Utils/Utils-MathXP/calcStats";
import { _getAll } from "../../../Utils/Utils-QueryXP/getAll";
import { _achivement, _stats } from "../../../Utils/Utils-UserXP/getAPI";
import { _upAll } from "../../../Utils/Utils-QueryXP/upAll";
import { errorBOT } from "../../../Interface/Error";
import { Command } from "../../../Interface/Command";
import { Context } from "../../../Config/Contex";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { pipe } from "fp-ts/lib/function";
import * as config from "../../../config.json";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _addXP } from "../../../Utils/Utils-LevelXP/XP";
import { _err } from "../../../Utils/Utils-Err&Log/ErrorCMD";
import { _countStats } from "../../../Utils/Utils-UserXP/embedStats";
import {
  _banErrorBOT,
  _userRegisterErrotBOT,
} from "../../../Utils/Utils-Err&Log/listErrorBOT";

// function -------------------------------------

const _claimRoles = (attributes: validateObj) =>
  pipe(
    _addXP(0, attributes.levelData),
    RTE.map(() => attributes)
  );

const _claimAchivement = (attributes: validateObj) =>
  pipe(
    _achivement(attributes.userXPData.xuid),
    RTE.map((achivement) => _calcAchivement(achivement, attributes)),
    RTE.chainFirst((obj) => _addXP(0, obj.levelData))
  );

const _claimProgress = (attributes: validateObj) =>
  pipe(
    _stats(attributes.userXPData.xuid),
    RTE.map((stats) => _calcStats(stats, attributes)),
    RTE.chainFirst((obj) => _addXP(0, obj.levelData))
  );

const _claimStats = (attributes: validateObj, msg: Message) => {
  const embed = new MessageEmbed()
    .setAuthor(config.BOT_Data.author)
    .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
    .setDescription(
      `Utente ${msg.author} hai chiamato il *traghettatore*\nAttendi che la transazione sia finita....`
    );
  return pipe(
    RTE.of(msg),
    RTE.chainTaskK((msg: Message) => _sendEmbed(msg, embed)),
    RTE.chain((msg: Message) =>
      pipe(
        _claimAchivement(attributes),
        RTE.chain((attributes: validateObj) => _claimProgress(attributes)),
        RTE.chain((attributes: validateObj) => _claimRoles(attributes)),
        RTE.chain((t) => _upAll(t)),
        RTE.chainFirstTaskK(() => () => msg.delete()),
        RTE.mapLeft((err) => {
          msg.delete();
          return err;
        })
      )
    )
  );
};

// export ---------------------------------------

export const _incassa: Command = {
  name: "incassa",
  description: "",
  syntax: `${config.BOT_Data.Prefix}incassa`,
  admin: false,
  execute(msg: Message, context: Context) {
    return pipe(
      _getAll(msg.author.id),
      RTE.chain<Context, errorBOT, resultObj, validateObj>(
        RTE.fromPredicate(
          (attributes): attributes is validateObj =>
            typeof attributes.levelData !== "undefined" &&
            typeof attributes.userXPData !== "undefined",
          (err) => _userRegisterErrotBOT(msg.author.id, err)
        )
      ),
      // RTE.chainFirstIOK(() => () => console.log(1)),
      RTE.chain<Context, errorBOT, validateObj, validateObj>(
        RTE.fromPredicate(
          (attributes): attributes is validateObj =>
            attributes.levelData.banned !== true,
          () => _banErrorBOT(msg.author.id)
        )
      ),
      RTE.chain((attributes: validateObj) =>
        pipe(
          _claimStats(attributes, msg),
          RTE.map((newAttributes: validateObj) =>
            _countStats(attributes, newAttributes)
          )
        )
      ),
      RTE.chainTaskK((embed: MessageEmbed) => _sendEmbed(msg, embed))
    );
  },
};
