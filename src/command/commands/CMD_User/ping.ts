// import ----------------------------------

import { Message, MessageEmbed } from "discord.js";
import { Context } from "../../../Config/Contex";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { error } from "fp-ts/lib/Console";
import { Command } from "../../../Interface/Command";
import * as config from "../../../config.json";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _addMoney } from "../../../Utils/Utils-User/addMoney";
import { _remMoney } from "../../../Utils/Utils-User/remMoney";

// function --------------------------------

const _cmd = (msg: Message) => {
  return pipe(
    RTE.Do,
    RTE.bind("context", () => RTE.ask<Context>()),
    RTE.chainTaskEitherK(({ context }) =>
      TE.tryCatch(
        () => msg.channel.send("pong"),
        () => error("Error on ping command")
      )
    )
  );
};

// export ----------------------------------

export const _ping: Command = {
  name: "ping",
  description: "simple ping command",
  syntax: `${config.BOT_Data.Prefix}ping`,
  admin: false,
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.of(msg),
      RTE.map(() => {
        // console.log(msg.channel.messages.cache.map((msg) => msg.author));
        const embed = new MessageEmbed()
          .setAuthor(config.BOT_Data.author)
          .setDescription(`Pong! <@!${config.XP_DATA.Role_Tallero}>`)
          .setColor(config.BOT_Data.CMD_Channel.CMD_Color);
        return embed;
      }),
      // RTE.chainFirst(() => _remMoney(msg.author.id, 99800100)),
      RTE.chainTaskK((embed) => _sendEmbed(msg, embed))
    );
  },
};
