// import ------------------------

import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import { _getAll } from "../../../Utils/Utils-QueryXP/getAll";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _getStats } from "../../../Utils/Utils-UserXP/getStats";
import { pipe } from "fp-ts/lib/function";
import { userXPAttribute } from "../../../DB/Models/userXPsTable";
import { createCanvas, loadImage } from "canvas";
import { Message, MessageAttachment, User } from "discord.js";
import { errorBOT } from "../../../Interface/Error";
import * as TE from "fp-ts/lib/TaskEither";
import * as config from "../../../config.json";
import {
  _sendAttachment,
  _sendEmbed,
} from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import {
  _canvaErrorBOT,
  _getDBDataErrorBOT,
} from "../../../Utils/Utils-Err&Log/listErrorBOT";

// function ----------------------

const _createCanvas = async (
  data: userXPAttribute,
  user: User
): Promise<MessageAttachment> => {
  // Create Canva
  const canvas = createCanvas(1920, 1080);

  const ctx = canvas.getContext("2d");
  // Get Image
  const background = await loadImage("./Img/SOT_IT_Statistiche.png");

  // Backgound
  ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

  ctx.strokeRect(0, 0, canvas.width, canvas.height);

  // Text font
  ctx.font = "80px sans-serif";
  ctx.fillStyle = "#ffffff";
  // Name
  ctx.textAlign = "center";
  ctx.fillText(`${user.username}`, 940, 228);

  // Resize font
  ctx.textAlign = "end";
  ctx.font = "60px sans-serif";
  // Treasure
  ctx.fillText(`${data.tresures}`, 540, 710);
  // Cargo
  ctx.fillText(`${data.loads}`, 540, 850);
  // Island
  ctx.fillText(`${data.isles}`, 540, 991);
  // Skulls
  ctx.fillText(`${data.skulls}`, 941, 710);
  // Vojage
  ctx.fillText(`${data.voyages}`, 941, 850);
  // Miles
  ctx.fillText(`${data.meters}`, 941, 991);

  // Resize font
  ctx.textAlign = "center";
  ctx.font = "120px sans-serif";
  // Achivement
  ctx.fillText(`${data.achievement}`, 1390, 820);

  // Circle Avatar
  ctx.beginPath();
  ctx.arc(232, 200, 175, 0, Math.PI * 2, true); // PosX, PosY, Arc, ni
  ctx.closePath();
  ctx.clip();
  const avatar = await loadImage(user.displayAvatarURL({ format: "png" }));
  ctx.drawImage(avatar, 58, 25, 362, 372); // PosX, PosY, Height, Weight

  // // BackupOld
  // ctx.beginPath();
  // ctx.arc(230, 195, 175, 0, Math.PI * 2, true);
  // ctx.closePath();
  // ctx.clip();
  // const avatar = await loadImage(user.displayAvatarURL({ format: "png" }));
  // ctx.drawImage(avatar, 48, 20, 372, 372);

  const final = new MessageAttachment(
    canvas.toBuffer(),
    `${user.username}_stats.png`
  );

  return final;
};

// export ------------------------

export const _statistiche: Command = {
  name: "statistiche",
  description: "Get Stats of users",
  syntax: `${config.BOT_Data.Prefix}statistiche`,
  admin: false,
  execute(msg: Message) {
    return pipe(
      msg.author.id,
      _getStats,
      RTE.chain<
        Context,
        errorBOT,
        userXPAttribute | undefined,
        userXPAttribute
      >(
        RTE.fromPredicate(
          (data): data is userXPAttribute => typeof data !== "undefined",
          (err): errorBOT => _getDBDataErrorBOT(msg.author.id, err)
        )
      ),
      RTE.chainTaskEitherK((data) =>
        TE.tryCatch(
          () => _createCanvas(data, msg.author),
          (err): errorBOT => _canvaErrorBOT(err)
        )
      ),
      RTE.chainTaskK((attachment) => _sendAttachment(msg, attachment))
    );
  },
};
