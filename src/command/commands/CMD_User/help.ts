// import --------------------------------

import { Message, MessageEmbed, User } from "discord.js";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import { cmds } from "../../cmdList";
import { pipe } from "fp-ts/lib/function";
import * as config from "../../../config.json";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import {
  _checkPermission,
  _cmdPermission,
} from "../../../Utils/Utils-CheckThings/checkPermission";
import { _descriptionHelp } from "../../../Doc/cmdDescription";

// function ------------------------------

const _sigleCMD = (call: string, cmds: Command[]) =>
  pipe(
    cmds.find((cmd) => cmd.name === call),
    (cmd) =>
      typeof cmd !== "undefined"
        ? new MessageEmbed()
            .setAuthor(config.BOT_Data.author)
            .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
            .setTitle(`❓ Guida al comando: ${String(cmd.name).toUpperCase()} ❔`)
            .setDescription(
              `**Sintassi**: ${cmd.syntax}\n**Descrizione**: \n${cmd.description}`
            )
        : new MessageEmbed()
            .setAuthor(config.BOT_Data.author)
            .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
            .setDescription(
              `⚠️ Comando non trovato, usa il comando **${config.BOT_Data.Prefix}help** per la lista completa`
            )
  );

const _allCMD = (cmds: Command[]) =>
  pipe(cmds, (cmds: Command[]) => {
    const embed = new MessageEmbed()
      .setAuthor(config.BOT_Data.author)
      .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
      .setDescription(
        `Ecco la lista dei comandi del nuovo *XP BOT*\nPer più informazioni prova il comando **${config.BOT_Data.Prefix}help name_cmd**`
      );
    cmds.forEach((cmd: Command) =>
      embed.addField(`🔸 Name: *${cmd.name}*`, `🔹 ${cmd.syntax}`, true)
    );
    return embed;
  });

const _filterCMD = (msg: Message, cmds: Command[]) =>
  pipe(_getFilter(msg), (filter) =>
    filter === null ? _allCMD(cmds) : _sigleCMD(filter, cmds)
  );

const _getFilter = (msg: Message) =>
  pipe(msg.content.split(" "), (arr: string[]) =>
    typeof arr[1] !== "undefined" ? arr[1] : null
  );

// export --------------------------------

export const _help: Command = {
  name: "help",
  description: _descriptionHelp,
  admin: false,
  syntax: "$help",
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.of(msg.author),
      RTE.map((user: User) =>
        pipe(_checkPermission(user.id), (check: boolean) =>
          check === false
            ? cmds.filter((cmd: Command) => cmd.admin === false)
            : cmds
        )
      ),
      RTE.map((cmds: Command[]) => _filterCMD(msg, cmds)),
      RTE.chainTaskK((embed: MessageEmbed) => _sendEmbed(msg, embed))
    );
  },
};
