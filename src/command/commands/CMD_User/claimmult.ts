// import ------------------------------

import { _payMoney } from "../../../Utils/Utils-User/payMoney";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../../../Config/Contex";
import * as config from "../../../config.json";
import { Command } from "../../../Interface/Command";
import { _descriptionClaimmult } from "../../../Doc/cmdDescription";
import { Message, MessageEmbed } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import { _checkUserBan } from "../../../Utils/Utils-CheckThings/checkUserBan";
import { errorBOT } from "../../../Interface/Error";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import { _multClaimedErrorBot } from "../../../Utils/Utils-Err&Log/listErrorBOT";
import { _setMult } from "../../../Utils/Utils-LevelXP/setMult";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { number } from "fp-ts";

// function ------------------------------

const _calcDiffHours = (attribute: levelAttribute): number => {
  const now_date = new Date().getTime();
  const future_date = new Date(Number(attribute.multTO)).getTime();
  const second = Math.floor((future_date - now_date) / 1000);
  const minutes = Math.floor(second / 60);
  const hours = Math.floor(minutes / 60);
  return hours;
};

const _calcDate = (attribute: levelAttribute) =>
  Number(attribute.multTO) === 0
    ? new Date().getTime() + 24 * 60 * 60 * 1000
    : Number(attribute.multTO) + 24 * 60 * 60 * 1000;

// export ------------------------------

export const _claimmult: Command = {
  name: "claimmult",
  syntax: `${config.BOT_Data.Prefix}claimmult`,
  admin: false,
  description: _descriptionClaimmult,
  execute(msg: Message, ctx: Context) {
    return pipe(
      _getUserLevel(msg.author.id),
      _checkUserBan(),
      RTE.chainFirst((attribute: levelAttribute) =>
        _payMoney(attribute.id, config.Money.Price_Mult)
      ),
      RTE.map((attribute: levelAttribute) => {
        const newattribute = {
          ...attribute,
          multTO: _calcDate(attribute),
          multRun: true,
        };
        return newattribute;
      }),
      RTE.chain<Context, errorBOT, levelAttribute, levelAttribute>(
        RTE.fromPredicate(
          (attribute: levelAttribute) => _calcDiffHours(attribute) <= 48,
          (attribute) => _multClaimedErrorBot(attribute)
        )
      ),
      RTE.chain((attribute: levelAttribute) => _setMult(attribute)),
      RTE.map((attribute: levelAttribute) => {
        const embed = new MessageEmbed()
          .setAuthor(config.BOT_Data.author)
          .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
          .setDescription(
            `Congratulazioni ${
              msg.author
            } moltiplicatore riscattato!\nAvrai un boost fino al giorno ${new Date(
              Number(attribute.multTO)
            ).toLocaleString("en-gb")}`
          );
        return embed;
      }),
      RTE.chainTaskK((embed) => _sendEmbed(msg, embed))
    );
  },
};
