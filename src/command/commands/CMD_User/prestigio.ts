// import ------------------------------

import { GuildMember, Message, MessageEmbed, Role } from "discord.js";
import { constVoid, pipe } from "fp-ts/lib/function";
import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import { errorBOT } from "../../../Interface/Error";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import { _add } from "../../../Utils/Utils-MathXP/simpleCalc";
import * as config from "../../../config.json";
import * as TE from "fp-ts/lib/TaskEither";
import { _calcLevel } from "../../../Utils/Utils-MathXP/calcLevel";
import { _calcNext } from "../../../Utils/Utils-MathXP/calcNext";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _remRoles } from "../../../Utils/Utils-LevelXP/remRoles";
import { _queryUPLevel } from "../../../Utils/Utils-LevelXP/upLevel";
import { _prestigeErrorBOT } from "../../../Utils/Utils-Err&Log/listErrorBOT";
import { _checkUserBan } from "../../../Utils/Utils-CheckThings/checkUserBan";
import { _checkMoney } from "../../../Utils/Utils-CheckThings/checkMoney";
import { _getMoney } from "../../../Utils/Utils-User/getMoney";
import { userAttribute } from "../../../DB/Models/usersTable";
import { _remMoney } from "../../../Utils/Utils-User/remMoney";
import { _addXP, _rankXPreset, _remXP } from "../../../Utils/Utils-LevelXP/XP";
import { _descriptionPrestigio } from "../../../Doc/cmdDescription";
import { _addDiscordRole } from "../../../Utils/Utils-DiscordAPI/rolesDiscordAPI";

// function ----------------------------

const _addRolePrestige = (msg: Message) =>
  pipe(
    TE.Do,
    TE.bind("member", () =>
      TE.fromTask(() => msg.guild!.members.fetch(msg.author.id))
    ),
    TE.bind("role", () =>
      TE.fromTask(() => msg.guild!.roles.fetch(config.XP_DATA.XP_Prestige))
    ),
    TE.chain(({ member, role }) =>
      role !== null
        ? pipe(
            _addDiscordRole(member, role),
            TE.map(() => constVoid)
          )
        : TE.of(constVoid)
    )
  );

const _payPrestige = (id: string) =>
  pipe(
    _getMoney(id),
    _checkMoney(config.Money.Price_prestige),
    RTE.chain((attribute: userAttribute) =>
      _remMoney(attribute.userId, config.Money.Price_prestige)
    )
  );

const _rankXP = (xp: number, oldMax: number): number => xp - oldMax;

const _downGrade = (attribute: levelAttribute) =>
  pipe(
    _rankXPreset(attribute.xp, attribute),
    RTE.map((attribute) => {
      return { ...attribute, rank: _add(attribute.rank, 1) };
    }),
    RTE.chain((newattribute: levelAttribute) =>
      _addXP(_rankXP(attribute.xp, attribute.next), newattribute)
    )
    // RTE.chainFirstIOK((attribute) => () => console.log(attribute))
  );

// export ------------------------------

export const _prestigio: Command = {
  name: "prestigio",
  description: _descriptionPrestigio,
  syntax: `${config.BOT_Data.Prefix}prestigio`,
  admin: false,
  execute(
    msg: Message,
    context: Context
  ): RTE.ReaderTaskEither<Context, errorBOT, Message> {
    return pipe(
      _getUserLevel(msg.author.id),
      _checkUserBan(),
      RTE.chain<Context, errorBOT, levelAttribute, levelAttribute>(
        RTE.fromPredicate(
          (attribute) => attribute.level === 100,
          (): errorBOT => _prestigeErrorBOT(msg.author)
        )
      ),
      RTE.chainFirst((attribute) => _payPrestige(attribute.id)),
      RTE.chain((attribute) => _downGrade(attribute)),
      RTE.chain((attribute) => _remRoles(attribute)),
      RTE.chain((attribute) => _queryUPLevel(attribute)),
      RTE.chainTaskEitherK(() => _addRolePrestige(msg)),
      RTE.map(() => {
        const embed = new MessageEmbed()
          .setAuthor(config.BOT_Data.author)
          .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
          .setTitle(`Prestigio effettuato!!!!`)
          .setDescription(`Congratulazioni pirata!`);
        return embed;
      }),
      RTE.chainTaskK((embed) => _sendEmbed(msg, embed))
    );
  },
};
