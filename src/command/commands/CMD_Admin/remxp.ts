// import -----------------------------------

import { Message, MessageEmbed, User } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { errorBOT } from "../../../Interface/Error";
import * as config from "../../../config.json";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import { _addXP, _remXP } from "../../../Utils/Utils-LevelXP/XP";
import { _queryUPLevel } from "../../../Utils/Utils-LevelXP/upLevel";
import { _cmdPermission } from "../../../Utils/Utils-CheckThings/checkPermission";
import { _syntaxErrorBOT } from "../../../Utils/Utils-Err&Log/listErrorBOT";
import { _checkAssignXP } from "../../../Utils/Utils-CheckThings/checkAssignXP";
import { _formatNumber } from "../../../Utils/Utils-MathXP/formatNumber";
import { _descriptionRemXP } from "../../../Doc/cmdDescription";

// function ---------------------------------

const _checkXP0 = (msg: Message) =>
  RTE.chain<Context, errorBOT, levelAttribute, levelAttribute>(
    RTE.fromPredicate(
      (attribute: levelAttribute) =>
        attribute.xp - Number(msg.content.split(" ")[1]) > 0,
      (attribute): errorBOT =>
        _syntaxErrorBOT(`Esperienza dell'utente <@!${attribute.id}> sotto 0`)
    )
  );

const _runAdminCMD = (msg: Message) =>
  pipe(
    RTE.of(msg.mentions.users.first()),
    _checkAssignXP(msg),
    RTE.chain((user: User) => _getUserLevel(user.id)),
    _checkXP0(msg),
    RTE.chain(
      (attribute: levelAttribute) =>
        _remXP(Number(msg.content.split(" ")[1]), attribute) // Take the number for msg and make it negative
    ),
    RTE.chain((attribute) => _queryUPLevel(attribute)),
    RTE.chainTaskK((attribute) => {
      const embed = new MessageEmbed()
        .setAuthor(config.BOT_Data.author)
        .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
        .setDescription(
          `Ex rimossa dall'utente <@!${attribute.id}>\nXP: ${_formatNumber(
            attribute.xp + Math.abs(Number(msg.content.split(" ")[1]))
          )} ------> ${_formatNumber(attribute.xp)}`
        );
      return _sendEmbed(msg, embed);
    })
  );

// export -----------------------------------

export const _rem_cmd: Command = {
  name: "remxp",
  description: _descriptionRemXP,
  syntax: `${config.BOT_Data.Prefix}remxp **num** *@tag*`,
  admin: true,
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.of(msg.author),
      _cmdPermission(),
      RTE.chain(() => _runAdminCMD(msg))
    );
  },
};
