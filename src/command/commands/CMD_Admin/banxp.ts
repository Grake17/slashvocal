// import -----------------------------

import { Message, MessageEmbed, User } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { errorBOT } from "../../../Interface/Error";
import * as config from "../../../config.json";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _queryUPLevel } from "../../../Utils/Utils-LevelXP/upLevel";
import { _cmdPermission } from "../../../Utils/Utils-CheckThings/checkPermission";
import { _syntaxErrorBOT } from "../../../Utils/Utils-Err&Log/listErrorBOT";
import { _descriptionBan } from "../../../Doc/cmdDescription";

// export -----------------------------

export const _ban_cmd: Command = {
  name: "ban",
  description: _descriptionBan,
  syntax: `${config.BOT_Data.Prefix}ban *@tag*`,
  admin: true,
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.of(msg.author),
      _cmdPermission(),
      RTE.map(() => msg.mentions.users.first()),
      RTE.chain<Context, errorBOT, User | undefined, User>(
        RTE.fromPredicate(
          (user): user is User => typeof user !== "undefined",
          (): errorBOT => _syntaxErrorBOT(`User is not defined`)
        )
      ),
      RTE.chain((user) => _getUserLevel(user.id)),
      RTE.chain<Context, errorBOT, levelAttribute, levelAttribute>(
        RTE.fromPredicate(
          (attribute) => attribute.banned !== true,
          (attribute): errorBOT =>
            _syntaxErrorBOT(`L'utente <@!${attribute.id}> è già bannato`)
        )
      ),
      RTE.chain((attribute: levelAttribute) =>
        _queryUPLevel({ ...attribute, banned: true })
      ),
      RTE.map(() => {
        const embed: MessageEmbed = new MessageEmbed()
          .setAuthor(config.BOT_Data.author)
          .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
          .setDescription(
            `L'utente ${msg.mentions.users.first()} è stato **bannato!**`
          );
        return embed;
      }),
      RTE.chainTaskK((embed: MessageEmbed) => _sendEmbed(msg, embed))
    );
  },
};
