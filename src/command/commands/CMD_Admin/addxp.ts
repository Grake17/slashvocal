// import ----------------------------

import { Message, MessageEmbed, User } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import { Context } from "../../../Config/Contex";
import { Command } from "../../../Interface/Command";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as config from "../../../config.json";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _queryUPLevel } from "../../../Utils/Utils-LevelXP/upLevel";
import { _addXP } from "../../../Utils/Utils-LevelXP/XP";
import { _getUserLevel } from "../../../Utils/Utils-LevelXP/getLevel";
import { levelAttribute } from "../../../DB/Models/levelsTable";
import { _sendLog } from "../../../Utils/Utils-Err&Log/Log";
import { _err } from "../../../Utils/Utils-Err&Log/ErrorCMD";
import { _cmdPermission } from "../../../Utils/Utils-CheckThings/checkPermission";
import { _syntaxErrorBOT } from "../../../Utils/Utils-Err&Log/listErrorBOT";
import { _checkAssignXP } from "../../../Utils/Utils-CheckThings/checkAssignXP";
import { _formatNumber } from "../../../Utils/Utils-MathXP/formatNumber";
import { _descriptionAddXP } from "../../../Doc/cmdDescription";

// function --------------------------

const _runAdminCMD = (msg: Message) =>
  pipe(
    RTE.of(msg.mentions.users.first()),
    _checkAssignXP(msg),
    RTE.chain((user: User) =>
      _assignXP(Number(msg.content.split(" ")[1]), user.id)
    ),
    RTE.chainTaskK((attribute) => {
      const embed = new MessageEmbed()
        .setAuthor(config.BOT_Data.author)
        .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
        .setDescription(
          `Ex aggiunta all'utente <@!${attribute.id}>\nXP: ${_formatNumber(
            attribute.xp - Math.abs(Number(msg.content.split(" ")[1]))
          )} ------> ${_formatNumber(attribute.xp)}`
        );
      return _sendEmbed(msg, embed);
    })
  );

const _assignXP = (xp: number, id: string) =>
  pipe(
    _getUserLevel(id),
    // RTE.chainFirstIOK(() => () => console.log(Math.abs(xp))),
    RTE.chain((attribute: levelAttribute) => _addXP(Math.abs(xp), attribute)),
    RTE.chain((attribute: levelAttribute) => _queryUPLevel(attribute))
  );

// export ----------------------------

export const _add_xp: Command = {
  name: "addxp",
  description: _descriptionAddXP,
  syntax: `${config.BOT_Data.Prefix}addxp **num** *@tag*`,
  admin: true,
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.of(msg.author),
      _cmdPermission(),
      RTE.chain(() => _runAdminCMD(msg))
    );
  },
};
