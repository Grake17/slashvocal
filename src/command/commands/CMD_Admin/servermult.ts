// import ----------------------------

import { Message, MessageEmbed } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import { Context } from "../../../Config/Contex";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Command } from "../../../Interface/Command";
import { _cmdPermission } from "../../../Utils/Utils-CheckThings/checkPermission";
import { errorBOT } from "../../../Interface/Error";
import { _syntaxErrorBOT } from "../../../Utils/Utils-Err&Log/listErrorBOT";
import * as fs from "fs";
import * as IO from "fp-ts/lib/IO";
import * as config from "../../../config.json";
import { IntConfig } from "../../../Interface/Config";
import { _sendEmbed } from "../../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _descriptionServermult } from "../../../Doc/cmdDescription";

// function --------------------------

const _writeOnJson = (n: number) =>
  pipe(
    IO.of(config),
    IO.map((config: IntConfig) => {
      return {
        ...config,
        XP_DATA: {
          ...config.XP_DATA,
          XP_Job: { ...config.XP_DATA.XP_Job, XP_Mult: n },
        },
      };
    }),
    IO.chain(
      (config: IntConfig) => () =>
        fs.writeFileSync(
          `./dist/config.json`,
          JSON.stringify(config, null, 2),
          {
            encoding: "utf-8",
          }
        )
    )
  );

// export ----------------------------

export const _servermult: Command = {
  name: "servermult",
  description: _descriptionServermult,
  syntax: `${config.BOT_Data.Prefix}servermult **num**`,
  admin: true,
  execute(msg: Message, context: Context) {
    return pipe(
      RTE.of(msg.author),
      _cmdPermission(),
      RTE.map(() => Number(msg.content.split(" ")[1])),
      RTE.chain<Context, errorBOT, number, number>(
        RTE.fromPredicate(
          (text) => !isNaN(text),
          (): errorBOT => _syntaxErrorBOT(`Non hai inserito un numero`)
        )
      ),
      // RTE.chainTaskEitherK((n: number) => _writeOnJson2(n)),
      RTE.map((n: number) => (context.mult = n)),
      RTE.chainIOK((n: number) => _writeOnJson(n)),
      RTE.map(() => {
        const embed = new MessageEmbed()
          .setAuthor(config.BOT_Data.author)
          .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
          .setDescription(
            `Il moltiplicatore del server ora è settato su: **x${Number(
              msg.content.split(" ")[1]
            )}**`
          );
        return embed;
      }),
      RTE.chainTaskK((embed: MessageEmbed) => _sendEmbed(msg, embed))
    );
  },
};
