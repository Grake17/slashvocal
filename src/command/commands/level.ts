// import ----------------------------

import { Message, MessageAttachment, MessageEmbed, User } from "discord.js";
import { pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { Command } from "../../Interface/Command";
import * as config from "../../config.json";
import { _getUserLevel } from "../../Utils/Utils-LevelXP/getLevel";
import { levelAttribute } from "../../DB/Models/levelsTable";
import { _sendEmbed } from "../../Utils/Utils-DiscordAPI/embedDiscordAPI";
import { _formatNumber } from "../../Utils/Utils-MathXP/formatNumber";
import { _descriptionLevel } from "../../Doc/cmdDescription";
import { Canvas, createCanvas, loadImage } from "canvas";
import { errorBOT } from "../../Interface/Error";
import { _canvaErrorBOT } from "../../Utils/Utils-Err&Log/listErrorBOT";
import { _checkRoleMulti } from "../../Utils/Utils-CheckThings/checkRoleMulti";

// function --------------------------

const _sameUser = (attribute: levelAttribute, msg: Message) =>
  TE.tryCatch(
    () => _levelCanvas(attribute, msg.author),
    (err): errorBOT => _canvaErrorBOT(err)
  );

const _otherUser = (attribute: levelAttribute) =>
  pipe(
    _checkRoleMulti(attribute.id),
    RTE.map((check) => {
      const text = [
        `👤 Profilo livello di: <@!${attribute.id}>`,
        `💰 XP: ${_formatNumber(attribute.xp)}`,
        `📏 Level: ${_formatNumber(attribute.level)}`,
        `🧮 Next: ${_formatNumber(attribute.next)}`,
        `📀 Rank: ${_formatNumber(attribute.rank)}`,
        `📩 N° Msg: ${_formatNumber(attribute.msgCount)}`,
        `☎️ N° Time: ${_formatNumber(attribute.voiceCount)}`,
        `❓ Status Message: ${attribute.msgRun ? "✅" : "❌"}`,
        `❓ Status Voice: ${attribute.voiceRun ? "✅" : "❌"}`,
        `❓ Moltiplicatore: ${attribute.multRun ? "✅" : "❌"}`,
        `❓ Server boost?: ${check ? "✅" : "❌"}`,
        `📆 Drop Moltiplicatore: ${
          attribute.multRun
            ? new Date(Number(attribute.multTO)).toLocaleString()
            : "Nessun moltiplicatore"
        }`,
        `🚫 Bannato?: ${attribute.banned}`,
      ].join("\n");
      const embed = new MessageEmbed()
        .setAuthor(config.BOT_Data.author)
        .setColor(config.BOT_Data.CMD_Channel.CMD_Color)
        .setDescription(text);
      return embed;
    })
  );

const _resiseName = (canvas: Canvas, name: string): string => {
  const ctx = canvas.getContext("2d");

  // Base Font Size
  let fontsize = 65;

  do {
    ctx.font = `${(fontsize -= 10)}px sans-serif`;
  } while (ctx.measureText(name).width > 470);

  return ctx.font;
};

const _levelCanvas = async (attribute: levelAttribute, user: User) => {
  const canvas = createCanvas(960, 540);

  const ctx = canvas.getContext("2d");

  // Get Image
  const background = await loadImage("./Img/SOT_IT_Livelli2.png");

  // BackGround
  ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

  // Text font
  ctx.font = _resiseName(canvas, user.username);
  ctx.fillStyle = "#ffffff";
  // Name
  ctx.textAlign = "center";
  ctx.fillText(`${user.username}`, 483, 117);

  // Resize font
  ctx.textAlign = "end";
  ctx.font = "55px sans-serif";
  // Level
  ctx.fillText(`${_formatNumber(attribute.level)}`, 450, 350);
  // Rank
  ctx.fillText(`${_formatNumber(attribute.rank)}`, 850, 350);
  // XP
  ctx.fillText(`${_formatNumber(attribute.xp)}`, 450, 490);
  // Next
  ctx.fillText(`${_formatNumber(attribute.next)}`, 850, 490);

  // Circle Avatar
  ctx.beginPath();
  ctx.arc(170, 98, 82, 0, Math.PI * 2, true); // PosX, PosY, Arc, ni
  ctx.closePath();
  ctx.clip();
  const avatar = await loadImage(user.displayAvatarURL({ format: "png" }));
  ctx.drawImage(avatar, 87, 16, 165, 165); // PosX, PosY, Height, Weight

  // Load Image
  const output = new MessageAttachment(
    canvas.toBuffer(),
    `${user.username}_stats.png`
  );

  return output;
};

// export ----------------------------

export const _level: Command = {
  name: "level",
  description: _descriptionLevel,
  syntax: `${config.BOT_Data.Prefix}level`,
  admin: false,
  execute(msg: Message) {
    return pipe(
      RTE.of(msg.mentions.users.first()),
      RTE.chain((user: User | undefined) =>
        typeof user?.id !== "undefined" &&
        config.Permission.find((obj) => obj.ID === msg.author.id)
          ? pipe(
              _getUserLevel(user.id),
              RTE.chain((attribute: levelAttribute) => _otherUser(attribute)),
              RTE.chainTaskK((embed) => _sendEmbed(msg, embed))
            )
          : pipe(
              _getUserLevel(msg.author.id),
              RTE.chainTaskEitherK((attribute: levelAttribute) =>
                _sameUser(attribute, msg)
              ),
              RTE.chainTaskK((attachment: MessageAttachment) =>
                _sendEmbed(msg, attachment)
              )
            )
      )
    );
  },
};
