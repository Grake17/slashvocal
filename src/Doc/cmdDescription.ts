// import --------------------------------------

import * as config from "../config.json";

// export --------------------------------------

export const _descriptionLevel = [
  `Il comando level fornisce all'utente la sua posizione nella scala dei livelli.`,
  `🔹**Livello**: livello corrente`,
  `🔸**Prestigio**: livello prestigio del pirata`,
  `🔹**XP**: esperienza corrente dell'utente`,
  `🔸**Successivo**: esperienza necessaria per il livello successivo`,
].join("\n");

export const _descriptionSyncXP = [
  `Questo comando serve per trasferire l'esperienza accumulata nel vecchio sistema a quello nuovo.`,
  `**Credit**: <@!${"402535288808931328"}>`,
].join("\n");

export const _descriptionStatus = [
  `Questo comando fornisce all'utente un riscontro di:`,
  `🔹 Quanti minuti si è rimasti in canale vocale`,
  `🔸 Quanti messaggi hanno fornito xp all'utente `,
  `🔹 Info sui moltiplicatori attivi`,
  `🔸 Moltiplicatore dell'utente sul server`,
  `🔹 Quanda XP si guadagna, la minuto sul server`,
].join("\n");

export const _descriptionPrestigio = [
  `Il comando Prestigio serve per alzare il rank dell'utente se questo è al livello massimo`,
  `Ecco cosa succederà quando si eseguirà il prestigio`,
  `🔹 L'utente guadagnerà **${config.Money.Rank_mult}** talleri in più a livello in base al prestigio ( Guadagno base: ${config.Money.Reward_next} )`,
  `🔹 L'utente otterrà il ruolo <@&${config.XP_DATA.XP_Prestige}>`,
  `🔸 Verrà sottratta all'utente l'xp necessaria per raggiungere il livello **${config.XP_DATA.XP_MaxLevel}**`,
  `🔸 Verranno rimossi dall'utente i seguenti ruoli ${config.XP_DATA.XP_Roles.map(
    (role) => `\n🔸<@&${role.ID_Role}>`
  )}`,
  `**NB**: Effettuare il prestigio costerà ${config.Money.Price_prestige} Talleri 💰`,
].join("\n");

export const _descriptionHelp = [
  `Il comando Help fornisce all'utente una spiegazione dattagliata sulle funzionalità dei vari comandi`,
  `Prova il comando **${config.BOT_Data.Prefix}help nome_comando** per più info!`,
].join("\n");

export const _descriptionClaimmult = [
  `Il comando claimmult, con un costo di **${config.Money.Price_Mult} Talleri**, aggiungerà **+1** al moltiplicatore personale`,
  `🔹 Il Moltiplicatore durera per 24h`,
  `🔸 Questa offerta è cumulabile solo 2 volte`,
].join("\n");

export const _descriptionClaimmultMensile = [
  `Il comando claimmensile, con un costo di **${config.Money.Price_Mult_Mensile} Talleri**, aggiungerà per 30 giorni **+1** al moltiplicatore personale`,
  `🔸 Questa offerta è cumulabile`,
].join("\n");

export const _descriptionBan = [
  `Il comando ban escluderà l'utente menzionato dal sistema impedendogli di guadagnare XP e Talleri`,
  `⚠️ Sono richiesti permessi amministrativi sul Bot per questo comando`,
].join("\n");

export const _descriptionUnBan = [
  `Il comando unban ripristinerà l'utente menzionato dal sistema permettendogli di guadagnare XP e Talleri `,
  `⚠️ Sono richiesti permessi amministrativi sul Bot per questo comando`,
].join("\n");

export const _descriptionServermult = [
  `Il comando servermult gestisce il moltiplicatore degli utenti di tutto il server`,
  `⚠️ Sono richiesti permessi amministrativi sul Bot per questo comando`,
].join("\n");

export const _descriptionAddXP = [
  `Il comando addxp aggiunge l'XP inserita all'utente menzionato`,
  `⚠️ Sono richiesti permessi amministrativi sul Bot per questo comando`,
].join("\n");

export const _descriptionRemXP = [
  `Il comando remxp rimuove l'XP inserita all'utente menzionato`,
  `⚠️ Sono richiesti permessi amministrativi sul Bot per questo comando`,
].join("\n");

export const _descriptionListaPremi = [
  `Il comando lista premi serve per elencare tutti i vari modi in cui si possono guadaganre **XP** e **Talleri** con **${config.BOT_Data.author}**`,
].join("\n");

export const _descriptionRegister = [
  `Il comando register manderà all'utente un link per la registrazione al nuovo **BOT XP**`,
  `Ecco cosa verrà chiesto all'utente`,
  `🔸 Accesso ad **Username**, **avatar** e **banner**`,
  `🔸 Accesso ai collegamenti di **account di terzi**`,
].join("\n");
