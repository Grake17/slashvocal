// import -------------------------

import { CronJob } from "cron";
import { flow, pipe } from "fp-ts/lib/function";
import { Context } from "../Config/Contex";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../Interface/Error";
import { _getAllDBDataErrorBOT } from "../Utils/Utils-Err&Log/listErrorBOT";
import * as A from "fp-ts/lib/Array";
import * as IO from "fp-ts/lib/IO";
import { levelAttribute } from "../DB/Models/levelsTable";
import { _queryUPLevel } from "../Utils/Utils-LevelXP/upLevel";
import { _errorJob } from "../Utils/Utils-Err&Log/errorJob";
import { _setMult } from "../Utils/Utils-LevelXP/setMult";

// function -----------------------

const _getAllMult = () =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.findAll({
              where: { multRun: true },
              transaction: t,
            });
            return data;
          });
          return result;
        },
        (err: unknown): errorBOT => _getAllDBDataErrorBOT("Mult Job", err)
      )
    ),
    RTE.map(A.map((data) => data.get()))
  );

const _start =
  (text: string): IO.IO<void> =>
  () =>
    console.log(text);

// export -------------------------

export const _multJob = (context: Context) => {
  _start(`[Job] Cron Mult XP avviato!`)();
  return new CronJob("* */1 * * *", () =>
    flow(
      _getAllMult,
      RTE.chain(
        RTE.traverseArray((attribute: levelAttribute) =>
          Date.now() >= attribute.multTO
            ? _setMult({ ...attribute, multRun: false, multTO: 0 })
            : RTE.of(attribute)
        )
      ),
      RTE.mapLeft((err: errorBOT) => _errorJob(err)(context)())
    )()(context)()
  );
};
