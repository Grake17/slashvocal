// import --------------------------------------

import { CronJob } from "cron";
import { flow, pipe } from "fp-ts/lib/function";
import * as RTE from "fp-ts/lib/ReaderTaskEither";
import { Context } from "../Config/Contex";
import * as TE from "fp-ts/lib/TaskEither";
import { errorBOT } from "../Interface/Error";
import { _getAllDBDataErrorBOT } from "../Utils/Utils-Err&Log/listErrorBOT";
import * as A from "fp-ts/lib/Array";
import * as IO from "fp-ts/lib/IO";
import * as config from "../config.json";
import { levelAttribute } from "../DB/Models/levelsTable";
import { _getDiscordUser } from "../Utils/Utils-DiscordAPI/usersDiscordAPI";
import { _getDiscordGuild } from "../Utils/Utils-DiscordAPI/guildDiscordAPI";
import { _remMgs } from "../Utils/Utils-LevelXP/setMgs";
import { Guild, GuildMember } from "discord.js";
import { _checkRoleMulti } from "../Utils/Utils-CheckThings/checkRoleMulti";
import { _randomXP, _addXP } from "../Utils/Utils-LevelXP/XP";
import { _queryUPLevel } from "../Utils/Utils-LevelXP/upLevel";
import { _addMoney } from "../Utils/Utils-User/addMoney";
import { _checkWarnRole } from "../Utils/Utils-CheckThings/checkWarnRole";

// function ------------------------------------

const _addXPJobVoice = (attribute: levelAttribute, context: Context) =>
  pipe(
    _checkRoleMulti(attribute.id),
    RTE.map(
      (test: boolean) =>
        _randomXP() *
        (context.mult + (test ? 1 : 0) + (attribute.multRun ? 1 : 0))
    ),
    // RTE.chainFirstIOK((xp) => () => console.log(xp, attribute.id)),
    RTE.chainFirst((xp: number) => _addMoney(attribute.id, xp)),
    RTE.chain((xp: number) => _addXP(xp, attribute)),
    RTE.map((attribute: levelAttribute) => {
      return { ...attribute, voiceCount: attribute.voiceCount + 1 };
    })
  );

const _addXPJobMsg = (attribute: levelAttribute, context: Context) =>
  pipe(
    _checkRoleMulti(attribute.id),
    RTE.map(
      (check: boolean) =>
        _randomXP() *
        (context.mult + (check ? 1 : 0) + (attribute.multRun ? 1 : 0))
    ),
    // RTE.chainFirstIOK((xp) => () => console.log(xp)),
    RTE.chain((xp: number) => _addXP(xp, attribute)),
    RTE.map((attribute: levelAttribute) => {
      return { ...attribute, msgRun: false, msgCount: attribute.msgCount + 1 };
    })
  );

const _queryAll = () =>
  pipe(
    RTE.ask<Context>(),
    RTE.chainTaskEitherK((context: Context) =>
      TE.tryCatch(
        async () => {
          const result = await context.sequelize.transaction(async (t) => {
            const data = await context.tables.level.findAll({
              where: { banned: false },
              transaction: t,
            });
            return data;
          });
          return result;
        },
        (err: unknown): errorBOT =>
          _getAllDBDataErrorBOT("Msg & Voice Job", err)
      )
    ),
    RTE.map(A.map((result) => result.get()))
  );

const _upJobUsers = (attributes: levelAttribute[], guild: Guild) =>
  pipe(
    RTE.ask<Context>(),
    RTE.chain((context: Context) =>
      pipe(
        RTE.of(attributes),
        RTE.chain(
          RTE.traverseArray((attribute: levelAttribute) =>
            pipe(
              _checkWarnRole(attribute.id),
              RTE.chain((check) =>
                check
                  ? RTE.of(attribute)
                  : pipe(
                      RTE.of(attribute),
                      RTE.chain((attribute: levelAttribute) =>
                        attribute.msgRun !== true
                          ? RTE.of(attribute)
                          : _addXPJobMsg(attribute, context)
                      ),
                      RTE.chain((attribute: levelAttribute) =>
                        pipe(
                          RTE.fromTaskEither(
                            _getDiscordUser(guild, attribute.id)
                          ),
                          RTE.chain((member: GuildMember) =>
                            member.voice.channelID === null ||
                            member.voice.mute === (true || null) ||
                            member.voice.selfDeaf === (null || true)
                              ? RTE.of(attribute)
                              : _addXPJobVoice(attribute, context)
                          )
                        )
                      ),
                      RTE.chain((newattribute: levelAttribute) =>
                        newattribute === attribute
                          ? RTE.of(attribute)
                          : _queryUPLevel(newattribute)
                      )
                    )
              )
            )
          )
        )
      )
    )
  );

const _start =
  (text: string): IO.IO<void> =>
  () =>
    console.log(text);

// export --------------------------------------

export const _MsgVoiceJob = (context: Context): CronJob => {
  _start(`[Job] Cron Msg & Voice avviato!`)();
  return new CronJob("*/3 * * * *", () =>
    flow(
      _queryAll,
      RTE.chain((attributes: levelAttribute[]) =>
        pipe(
          RTE.fromTaskEither(_getDiscordGuild(context, context.env.GUILD_ID)),
          RTE.chain((guild: Guild) => _upJobUsers(attributes, guild))
        )
      )
    )()(context)()
  );
};
